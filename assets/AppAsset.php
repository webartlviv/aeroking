<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',

        'libs/animate/animate.css',
        'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css',
        'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css',
        'js/owlcarousel/owl.carousel.min.css',
        'js/owlcarousel/owl.theme.default.min.css',
        'css/adminStyle.css',
    ];
    public $js = [
        // 'libs/modernizr/modernizr.js',
        // 'libs/jquery/jquery-1.11.2.min.js',
        'libs/waypoints/waypoints.min.js',
        'libs/animate/animate-css.js',
        'libs/plugins-scroll/plugins-scroll.js',
        'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js',
        'js/owlcarousel/owl.carousel.min.js',
        'js/jquery.maskedinput.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
    // public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
}
