<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\Pagination;
use app\models\Clients;
use app\models\UploadForm;
use yii\web\UploadedFile;

class AdminController extends \yii\web\Controller
{
	public $layout = 'admin';

	public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
    	if (!Yii::$app->user->isGuest) {
            return $this->render('index');
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->render('index');
        }
        return $this->render('login', [
            'model' => $model,
        ]);
        //return $this->render('index');
    }
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionClients()
    {
    	$model = new \app\models\Clients;
        $query = $model->find();

        $pagination = new Pagination([
            'defaultPageSize' => 100,
            'totalCount' => $query->count(),
        ]);

        $clients = $query->orderBy('id')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        if(isset($_GET['add'])){
            $model = new \app\models\ClientsDataForm;
            if ($model->load(Yii::$app->request->post())) {
                if($model->addData()){
                    $model = new \app\models\Clients;
                    $query = $model->find();

                    $pagination = new Pagination([
                            'defaultPageSize' => 100,
                            'totalCount' => $query->count(),
                        ]);

                    $clients = $query->orderBy('id')
                        ->offset($pagination->offset)
                        ->limit($pagination->limit)
                        ->all();

                    return $this->render('clients', [
                        'clients' => $clients,
                        'pagination' => $pagination,
                    ]);
                }
            }
            return $this->render('ClientsData', [
                'model' => $model,
                'submitName' => 'Додати'
            ]);
        }else if(isset($_GET['edit'])){
            if(isset($_GET['id'])){
                $model = new \app\models\ClientsDataForm;
                if ($model->load(Yii::$app->request->post())) {
                    if($model->editData($_GET['id'])){
                        $model = new \app\models\Clients;
                        $query = $model->find();

                        $pagination = new Pagination([
                                'defaultPageSize' => 100,
                                'totalCount' => $query->count(),
                            ]);

                        $clients = $query->orderBy('id')
                            ->offset($pagination->offset)
                            ->limit($pagination->limit)
                            ->all();

                        return $this->render('clients', [
                            'clients' => $clients,
                            'pagination' => $pagination,
                        ]);
                    }
                }
                $clientData = $query->where(['id' => "{$_GET['id']}"])->one();
            
                return $this->render('ClientsData', [
                    'model' => $model,
                    'clientData' => $clientData,
                    'submitName' => 'Зберегти'
                ]);
            }else{
                return $this->render('clients', [
                        'clients' => $clients,
                        'pagination' => $pagination,
                    ]);
            }
        }else{
            return $this->render('clients', [
                'clients' => $clients,
                'pagination' => $pagination,
            ]);
        }
    }
    public function actionCategories()
    {
        $model = new \app\models\Categories;
        $query = $model->find();

        $pagination = new Pagination([
                'defaultPageSize' => 100,
                'totalCount' => $query->count(),
            ]);

        $categories = $query->orderBy('id')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        if(isset($_GET['add'])){
            $model = new \app\models\ProductsCategoriesDataForm;
            if ($model->load(Yii::$app->request->post())) {
                $model->image = UploadedFile::getInstance($model, 'image');
                if($model->addData()){
                    $model = new \app\models\Categories;
                    $query = $model->find();

                    $pagination = new Pagination([
                            'defaultPageSize' => 100,
                            'totalCount' => $query->count(),
                        ]);

                    $categories = $query->orderBy('id')
                        ->offset($pagination->offset)
                        ->limit($pagination->limit)
                        ->all();

                    return $this->render('categories', [
                        'categories' => $categories,
                        'pagination' => $pagination,
                    ]);
                }
            }
            return $this->render('ProductsCategoriesData', [
                'model' => $model,
                'categories' => $categories,
                'submitName' => 'Додати',
            ]);
        }else if(isset($_GET['edit'])){
            if(isset($_GET['id'])){
                $model = new \app\models\ProductsCategoriesDataForm;
                if ($model->load(Yii::$app->request->post())) {
                    $model->image = UploadedFile::getInstance($model, 'image');
                    if($model->editData($_GET['id'])){
                        $model = new \app\models\Categories;
                        $query = $model->find();

                        $pagination = new Pagination([
                                'defaultPageSize' => 100,
                                'totalCount' => $query->count(),
                            ]);

                        $categories = $query->orderBy('id')
                            ->offset($pagination->offset)
                            ->limit($pagination->limit)
                            ->all();

                        return $this->render('categories', [
                            'categories' => $categories,
                            'pagination' => $pagination,
                        ]);
                    }
                }
                $categoryData = $query->where(['id' => "{$_GET['id']}"])->one();
            
                return $this->render('ProductsCategoriesData', [
                    'model' => $model,
                    'categories' => $categories,
                    'categoryData' => $categoryData,
                    'submitName' => 'Зберегти',
                ]);
            }else{
                return $this->render('category', [
                        'category' => $categories,
                        'pagination' => $pagination,
                    ]);
            }
        }else{

            return $this->render('categories', [
                'categories' => $categories,
                'pagination' => $pagination,
            ]);
        }
    }
    public function actionProducts()
    {
        $modelUnits = new \app\models\Units;
        $queryUnits = $modelUnits->find();
        $units = $queryUnits->orderBy('id')
            ->all();

        foreach ($units as $unit) {
            $unitsName[$unit->id] = $unit->name;
        }

    	$modelCategories = new \app\models\Categories;
        $queryCategories = $modelCategories->find();
        $categories = $queryCategories->orderBy('id')
            ->all();

        foreach ($categories as $category) {
            $categoryName[$category->id] = $category->name;
        }

    	$modelProducts = new \app\models\Products;
    	$queryProducts = $modelProducts->find();

        $pagination = new Pagination([
            'defaultPageSize' => 100,
            'totalCount' => $queryProducts->count(),
        ]);

        $products = $queryProducts->orderBy('id')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        if(isset($_GET['add'])){
            $model = new \app\models\ProductsDataForm;
            if ($model->load(Yii::$app->request->post())) {
                $model->image = UploadedFile::getInstance($model, 'image');
                if($model->addData()){
                    $model = new \app\models\Products;
                    $query = $model->find();

                    $pagination = new Pagination([
                            'defaultPageSize' => 100,
                            'totalCount' => $query->count(),
                        ]);

                    $products = $query->orderBy('id')
                        ->offset($pagination->offset)
                        ->limit($pagination->limit)
                        ->all();

                    return $this->render('products', [
                        'products' => $products,
                        'categoryName' => $categoryName,
                        'pagination' => $pagination,
                    ]);
                }
            }
            return $this->render('ProductsData', [
                'model' => $model,
                'categories' => $categoryName,
                'units' => $unitsName,
                'submitName' => 'Додати',
            ]);
        }else if(isset($_GET['edit'])){
            if(isset($_GET['id'])){
                $model = new \app\models\ProductsDataForm;
                if ($model->load(Yii::$app->request->post())) {
                    $model->image = UploadedFile::getInstance($model, 'image');
                    if($model->editData($_GET['id'])){
                        $model = new \app\models\Products;
                        $query = $model->find();

                        $pagination = new Pagination([
                                'defaultPageSize' => 100,
                                'totalCount' => $query->count(),
                            ]);

                        $products = $query->orderBy('id')
                            ->offset($pagination->offset)
                            ->limit($pagination->limit)
                            ->all();

                        return $this->render('products', [
                            'products' => $products,
                            'categoryName' => $categoryName,
                            'pagination' => $pagination,
                        ]);
                    }
                }
                $productData = $queryProducts->where(['id' => "{$_GET['id']}"])->one();
            
                return $this->render('ProductsData', [
                    'model' => $model,
                    'productData' => $productData,
                    'categories' => $categoryName,
                    'units' => $unitsName,
                    'submitName' => 'Зберегти',
                ]);
            }else{
                return $this->render('products', [
                        'products' => $products,
                        'categoryName' => $categoryName,
                        'pagination' => $pagination,
                    ]);
            }
        }else{

            return $this->render('products', [
                'products' => $products,
                'categoryName' => $categoryName,
                'pagination' => $pagination,
            ]);
        }
    }
    public function actionRequests()
    {
    	$model = new \app\models\Requests;
        $query = $model->find();

        $pagination = new Pagination([
            'defaultPageSize' => 100,
            'totalCount' => $query->count(),
        ]);

        $requests = $query->orderBy('id')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        if(isset($_GET['add'])){
            $model = new \app\models\RequestsDataForm;
            if ($model->load(Yii::$app->request->post())) {
                if($model->addData()){
                    $model = new \app\models\Requests;
                    $query = $model->find();

                    $pagination = new Pagination([
                            'defaultPageSize' => 100,
                            'totalCount' => $query->count(),
                        ]);

                    $requests = $query->orderBy('id')
                        ->offset($pagination->offset)
                        ->limit($pagination->limit)
                        ->all();

                    return $this->render('requests', [
                        'requests' => $requests,
                        'pagination' => $pagination,
                    ]);
                }
            }
            return $this->render('RequestsData', [
                'model' => $model,
                'submitName' => 'Додати'
            ]);
        }else if(isset($_GET['edit'])){
            if(isset($_GET['id'])){
                $model = new \app\models\RequestsDataForm;
                if ($model->load(Yii::$app->request->post())) {
                    if($model->editData($_GET['id'])){
                        $model = new \app\models\Requests;
                        $query = $model->find();

                        $pagination = new Pagination([
                                'defaultPageSize' => 100,
                                'totalCount' => $query->count(),
                            ]);

                        $requests = $query->orderBy('id')
                            ->offset($pagination->offset)
                            ->limit($pagination->limit)
                            ->all();

                        return $this->render('requests', [
                            'requests' => $requests,
                            'pagination' => $pagination,
                        ]);
                    }
                }
                $requestData = $query->where(['id' => "{$_GET['id']}"])->one();
            
                return $this->render('RequestsData', [
                    'model' => $model,
                    'requestData' => $requestData,
                    'submitName' => 'Зберегти'
                ]);
            }else{
                return $this->render('requests', [
                        'requests' => $requests,
                        'pagination' => $pagination,
                    ]);
            }
        }else{
            return $this->render('requests', [
                'requests' => $requests,
                'pagination' => $pagination,
            ]);
        }
    }
    public function actionComments()
    {
        $model = new \app\models\Comments;
        $query = $model->find();

        $pagination = new Pagination([
                'defaultPageSize' => 100,
                'totalCount' => $query->count(),
            ]);

        $comments = $query->orderBy('id')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        if(isset($_GET['add'])){
            $model = new \app\models\CommentsDataForm;
            if ($model->load(Yii::$app->request->post())) {
                $model->image = UploadedFile::getInstance($model, 'image');
                if($model->addData()){
                    $model = new \app\models\Comments;
                    $query = $model->find();

                    $pagination = new Pagination([
                            'defaultPageSize' => 100,
                            'totalCount' => $query->count(),
                        ]);

                    $comments = $query->orderBy('id')
                        ->offset($pagination->offset)
                        ->limit($pagination->limit)
                        ->all();

                    return $this->render('comments', [
                        'comments' => $comments,
                        'pagination' => $pagination,
                    ]);
                }
            }
            return $this->render('CommentsData', [
                'model' => $model,
                'submitName' => 'Додати'
            ]);
        }else if(isset($_GET['edit'])){
            if(isset($_GET['id'])){
                $model = new \app\models\CommentsDataForm;
                if ($model->load(Yii::$app->request->post())) {
                    $model->image = UploadedFile::getInstance($model, 'image');
                    if($model->editData($_GET['id'])){
                        $model = new \app\models\Comments;
                        $query = $model->find();

                        $pagination = new Pagination([
                                'defaultPageSize' => 100,
                                'totalCount' => $query->count(),
                            ]);

                        $comments = $query->orderBy('id')
                            ->offset($pagination->offset)
                            ->limit($pagination->limit)
                            ->all();

                        return $this->render('comments', [
                            'comments' => $comments,
                            'pagination' => $pagination,
                        ]);
                    }
                }
                $commentData = $query->where(['id' => "{$_GET['id']}"])->one();
            
                return $this->render('CommentsData', [
                    'model' => $model,
                    'commentData' => $commentData,
                    'submitName' => 'Зберегти'
                ]);
            }else{
                return $this->render('comments', [
                        'comments' => $comments,
                        'pagination' => $pagination,
                    ]);
            }
        }else{
            return $this->render('comments', [
                'comments' => $comments,
                'pagination' => $pagination,
            ]);
        }
    }
}
