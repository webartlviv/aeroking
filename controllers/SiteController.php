<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function monthRu($month){
        switch ($month){
            case '01': $m='января'; break;
            case '02': $m='февраля'; break;
            case '03': $m='марта'; break;
            case '04': $m='апреля'; break;
            case '05': $m='мая'; break;
            case '06': $m='июня'; break;
            case '07': $m='июля'; break;
            case '08': $m='августа'; break;
            case '09': $m='сентября'; break;
            case '10': $m='октября'; break;
            case '11': $m='ноября'; break;
            case '12': $m='декабря'; break;
            default: $m=$month;
        }
        return $m;
    }
    public function month($month){
        switch ($month){
            case '01': $m='січня'; break;
            case '02': $m='лютого'; break;
            case '03': $m='березня'; break;
            case '04': $m='квітня'; break;
            case '05': $m='травня'; break;
            case '06': $m='червня'; break;
            case '07': $m='липня'; break;
            case '08': $m='серпня'; break;
            case '09': $m='вересня'; break;
            case '10': $m='жовтня'; break;
            case '11': $m='листопада'; break;
            case '12': $m='грудня'; break;
            default: $m=$month;
        }
        return $m;
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        Yii::$app->language = 'uk';
        $discount = '30%';
        $date = date_create(date('Y-m-d'));
        date_modify($date, '+3 day');
        $discountDate = date_format($date, 'j').' '.self::month(date_format($date, 'm'));

        $modelTexts = new \app\models\Texts;
        $queryTexts = $modelTexts->find();

        $texts = $queryTexts->orderBy('id')
            ->where(['lang' => 'ua'])
            ->all();

        foreach ($texts as $text) {
            $code = $text['code'];
            $text = $text['text'];
            $langText[$code] = $text;
        }

        $modelContactForm = new \app\models\ContactForm;
        if ($modelContactForm->load(Yii::$app->request->post())) {
            if($modelContactForm->newRequest()){
                 $messageAlert = $langText['alertRequestSuccess'];
            }
        }

        $modelBuyForm = new \app\models\BuyForm;
        if ($modelBuyForm->load(Yii::$app->request->post())) {
            if($modelBuyForm->newRequest()){
                $messageAlert = $langText['alertBuySuccess'];
            }
        }

        $modelCategories = new \app\models\Categories;
        $queryCategories = $modelCategories->find();

        $categories = $queryCategories->orderBy('id')
            ->where(['active' => '1'])
            ->all();

        foreach ($categories as $category) {
            $categoryName[$category->id] = $category->name;
        }

        $modelProducts = new \app\models\Products;
        $queryProducts = $modelProducts->find();

        $productsQuery = $queryProducts->orderBy('position')
            ->where(['active' => '1'])
            ->all();

        foreach ($productsQuery as $product) {
            $category = $product->category_id;
            $products[$category][] = $product;
        }

        $uniqueProducts = $queryProducts->orderBy('id')
            ->where(['unique' => '1', 'active' => '1'])
            ->all();

        $modelColors = new \app\models\Colors;
        $queryColors = $modelColors->find();

        $colors = $queryColors->orderBy('id')
            ->all();

        $modelUnits = new \app\models\Units;
        $queryUnits = $modelUnits->find();

        $modelColors = new \app\models\Colors;
        $queryColors = $modelColors->find();

        $modelUnits = new \app\models\Units;
        $queryUnits = $modelUnits->find();

        $unitsQuery = $queryUnits->orderBy('id')
            ->all();

        foreach ($unitsQuery as $key => $unit) {
            $units[$unit->id]['name'] = $unit->name;
            $units[$unit->id]['sort_name'] = $unit->sort_name;
        }

        $modelComments = new \app\models\Comments;
        $queryComments = $modelComments->find();

        $comments = $queryComments->orderBy('id')
            ->where(['active' => '1'])
            ->all();
        if(!isset($messageAlert)) $messageAlert = '';
        return $this->render('index', [
            'langText' => $langText,
            'colors' => $colors,
            'categories' => $categories,
            'categoryName' => $categoryName,
            'products' => $products,
            'uniqueProducts' => $uniqueProducts,
            'comments' => $comments,
            'units' => $units,
            'discount' => $discount,
            'discountDate' => $discountDate,
            'ancor' => null,
            'messageAlert' => $messageAlert,
        ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
