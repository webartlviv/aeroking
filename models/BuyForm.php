<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class BuyForm extends Model
{
    public $name;
    public $phone;
    public $count;

    public $product;
    public $price;
    public $napovn;
    public $color;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'phone', 'count'], 'required'],
            ['count', 'number'],
            [['product', 'price', 'napovn', 'color'], 'string'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Verification Code',
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $email the target email address
     * @return bool whether the model passes validation
     */
    public function newRequest()
    {
        if ($this->validate()) {
                $model = new \app\models\Requests;
                $model->name = $this->name;
                $model->phone = $this->phone;
                $orderSumm = $this->count * $this->price;
                $order = [ '0' => [
                    'Name' => $this->product,
                    'Count' => $this->count,
                    'Price' => $this->price,
                    'Napovn' => $this->napovn,
                    'Color' => $this->color,
                    'Summ' => $orderSumm,
                    ]
                ];
                $model->order = json_encode($order);
                $model->status = 1;
                $model->save();

                $text = "
                    Ім'я: {$this->name}
                    Телефон: {$this->phone}

                    Замовлення
                    Товар: {$this->product}
                    Кількість: {$this->count}
                    Ціна: {$this->price}
                    Наповнення: {$this->napovn}
                    Колір: {$this->color}

                    Сума замовлення: {$orderSumm} грн.
                ";

                Yii::$app->mailer->compose()
                ->setTo('aeroking.lviv@gmail.com')
                ->setFrom(['noreply@aeroking.com.ua' => 'Гелеві Кулі Львів - AeroKing'])
                ->setSubject('Нова покупка на сайті')
                ->setTextBody($text)
                ->send();
            return true;
        }
        return false;
    }
}
