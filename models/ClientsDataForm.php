<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class ClientsDataForm extends Model
{
    public $name;
    public $surname;
    public $phone;
    public $email;
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['name', 'phone'], 'required'],
            [['surname', 'email'], 'string'],
        ];
    }

    public function addData(){
        if ($this->validate()) {
            $model = new \app\models\Clients;
            $model->name = $this->name;
            $model->surname = $this->surname;
            $model->email = $this->email;
            $model->phone = $this->phone;
            // сохраняем запись, за место метода save() можно использовать метод insert() ($model->insert())
            $model->save();  
            return true;
        }
        return false;
    }

    public function editData($id){
        if ($this->validate()) {
            $commentModel = new \app\models\Clients;
            $model = $commentModel->find()->where("`id` = {$id}")->one();
            
            $model->name = $this->name;
            $model->surname = $this->surname;
            $model->email = $this->email;
            $model->phone = $this->phone;
            // сохраняем запись, за место метода save() можно использовать метод insert() ($model->insert())
            $model->save();  
            return true;
        }
        return false;
    }
}
