<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class CommentsDataForm extends Model
{
    public $name;
    public $comment;
    public $image;
    public $imageFile = true;
    public $from;
    public $active = true;
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['name', 'comment', 'from'], 'required'],
            [['image'], 'image', 'extensions' => 'png, jpg, jpeg, gif', 'skipOnEmpty' => true],
            ['active', 'boolean'],
        ];
    }

    public function addData(){
        if ($this->validate()) {
            $fileName = $this->image->baseName . '.' . $this->image->extension;
            $this->image->saveAs('img/comments/' . $fileName);
            
            $model = new \app\models\Comments;
            $model->name = $this->name;
            $model->comment = $this->comment;
            $model->image =  $fileName;
            $model->from = $this->from;
            $model->active = $this->active;
            // сохраняем запись, за место метода save() можно использовать метод insert() ($model->insert())
            $model->save();  
            return true;
        }
        return false;
    }

    public function editData($id){
        if ($this->validate()) {
            $commentModel = new \app\models\Comments;
            $model = $commentModel->find()->where("`id` = {$id}")->one();

            if(isset($this->image->baseName)){
                $fileName = $this->image->baseName . '.' . $this->image->extension;
                $this->image->saveAs('img/comments/' . $fileName);

                 $model->image =  $fileName;
            }

            $model->name = $this->name;
            $model->comment = $this->comment;
            $model->from = $this->from;
            $model->active = $this->active;
            // сохраняем запись, за место метода save() можно использовать метод insert() ($model->insert())
            $model->save();  
            return true;
        }
        return false;
    }
}
