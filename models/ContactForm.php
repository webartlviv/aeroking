<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $phone;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['name', 'phone'], 'required'],
        ];
    }

    /**
     * @return array customized attribute labels
     */

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $email the target email address
     * @return bool whether the model passes validation
     */
    public function newRequest()
    {
        if ($this->validate()) {
            $model = new \app\models\Requests;
            $model->name = $this->name;
            $model->phone = $this->phone;
            $model->status = 1;
            $model->save();

            $text = "
                Ім'я: {$this->name}
                Телефон: {$this->phone}
            ";

            Yii::$app->mailer->compose()
            ->setTo('aeroking.lviv@gmail.com')
            ->setFrom(['noreply@aeroking.com.ua' => 'Гелеві кулі Львів - AeroKing'])
            ->setSubject('Нова заявка на дзвінок з сайту')
            ->setTextBody($text)
            ->send();

            return true;
        }
        return false;
    }
}
