<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "products".
 *
 * @property integer $id
 * @property integer $category_id
 * @property string $name
 * @property string $description
 * @property string $price
 * @property integer $unit
 * @property string $image
 * @property integer $unique
 * @property string $special
 * @property integer $position
 * @property integer $active
 */
class Products extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'unit', 'unique', 'position', 'active'], 'integer'],
            [['price'], 'number'],
            [['special'], 'string'],
            [['name', 'description', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category ID',
            'name' => 'Name',
            'description' => 'Description',
            'price' => 'Price',
            'unit' => 'Unit',
            'image' => 'Image',
            'unique' => 'Unique',
            'special' => 'Special',
            'position' => 'Position',
            'active' => 'Active',
        ];
    }
}
