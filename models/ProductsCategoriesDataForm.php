<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class ProductsCategoriesDataForm extends Model
{
    public $name;
    public $image;
    public $position;
    public $active = true;
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['active', 'position'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['image'], 'image', 'extensions' => 'png, jpg, jpeg, gif', 'skipOnEmpty' => true],
        ];
    }

    public function addData(){
        if ($this->validate()) {
            $fileName = $this->image->baseName . '.' . $this->image->extension;
            $this->image->saveAs('img/categories/' . $fileName);
            
            $model = new \app\models\Categories;
            $model->name = $this->name;
            $model->image =  $fileName;
            $model->active = $this->active;
            // сохраняем запись, за место метода save() можно использовать метод insert() ($model->insert())
            $model->save();  
            return true;
        }
        return false;
    }

    public function editData($id){
        if ($this->validate()) {
            $categoryModel = new \app\models\Categories;
            $model = $categoryModel->find()->where("`id` = {$id}")->one();

            if(isset($this->image->baseName)){
                $fileName = $this->image->baseName . '.' . $this->image->extension;
                $this->image->saveAs('img/categories/' . $fileName);

                 $model->image =  $fileName;
            }

            $model->name = $this->name;
            $model->active = $this->active;
            // сохраняем запись, за место метода save() можно использовать метод insert() ($model->insert())
            $model->save();  
            return true;
        }
        return false;
    }
}
