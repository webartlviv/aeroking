<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class ProductsDataForm extends Model
{
    public $name;
    public $category_id;
    public $description;
    public $price;
    public $unit;
    public $image;
    public $unique;
    public $special;
    public $position;
    public $active;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['category_id', 'unit', 'position', 'active', 'unique'], 'integer'],
            [['price'], 'number'],
            [['special'], 'string'],
            [['name', 'description'], 'string', 'max' => 255],
            [['image'], 'image', 'extensions' => 'png, jpg, jpeg, gif', 'skipOnEmpty' => true],
        ];
    }

    public function addData(){
        if ($this->validate()) {
            $fileName = $this->image->baseName . '.' . $this->image->extension;
            $this->image->saveAs('img/comments/' . $fileName);
            
            $model = new \app\models\Products;
            $model->name = $this->name;
            $model->description = $this->description;
            $model->unit = $this->unit;
            $model->price = $this->price;
            $model->category_id = $this->category_id;
            $model->unique = $this->unique;
            $model->image =  $fileName;
            $model->active = $this->active;
            // сохраняем запись, за место метода save() можно использовать метод insert() ($model->insert())
            $model->save();  
            return true;
        }
        return false;
    }

    public function editData($id){
        if ($this->validate()) {
            $productModel = new \app\models\Products;
            $model = $productModel->find()->where("`id` = {$id}")->one();

            if(isset($this->image->baseName)){
                $fileName = $this->image->baseName . '.' . $this->image->extension;
                $this->image->saveAs('img/products/' . $fileName);

                 $model->image =  $fileName;
            }

            $model->name = $this->name;
            $model->description = $this->description;
            $model->unit = $this->unit;
            $model->price = $this->price;
            $model->category_id = $this->category_id;
            $model->unique = $this->unique ? 1 : 0;
            $model->active = $this->active ? 1 : 0;
            // сохраняем запись, за место метода save() можно использовать метод insert() ($model->insert())
            $model->save();  
            return true;
        }
        return false;
    }
}
