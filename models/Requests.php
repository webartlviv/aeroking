<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "requests".
 *
 * @property integer $id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property integer $status
 * @property string $delivery
 * @property string $comment
 * @property string $order
 * @property string $date
 */
class Requests extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'requests';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['delivery', 'order'], 'string'],
            [['date'], 'safe'],
            [['name', 'phone', 'email', 'comment'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'phone' => 'Phone',
            'email' => 'Email',
            'status' => 'Status',
            'delivery' => 'Delivery',
            'comment' => 'Comment',
            'order' => 'Order',
            'date' => 'Date',
        ];
    }
}
