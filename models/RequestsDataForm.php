<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class RequestsDataForm extends Model
{
    public $name;
    public $phone;
    public $email;
    public $status;
    public $delivery;
    public $comment;
    public $order;
    public $date;
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['name', 'phone', 'order', 'status'], 'required'],
            [['email', 'delivery', 'comment', 'order'], 'string'],
        ];
    }

    public function addData(){
        if ($this->validate()) {
            $model = new \app\models\Requests;
            $model->name = $this->name;
            $model->phone = $this->phone;
            $model->email = $this->email;
            $model->delivery = $this->delivery;
            $model->comment = $this->comment;
            $model->order = $this->order;
            $model->status = $this->status;
            // сохраняем запись, за место метода save() можно использовать метод insert() ($model->insert())
            $model->save(); 
            return true;
        }
        return false;
    }

    public function editData($id){
        if ($this->validate()) {
            $requestModel = new \app\models\Requests;
            $model = $requestModel->find()->where("`id` = {$id}")->one();

            $model->name = $this->name;
            $model->phone = $this->phone;
            $model->email = $this->email;
            $model->delivery = $this->delivery;
            $model->comment = $this->comment;
            $model->order = $this->order;
            $model->status = $this->status;
            $model->data = date('Y-m-d H:i:s');
            // сохраняем запись, за место метода save() можно использовать метод insert() ($model->insert())
            $model->save(); 
            return true;
        }
        return false;
    }
}
