<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "texts".
 *
 * @property integer $id
 * @property string $code
 * @property string $text
 * @property string $lang
 */
class Texts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'texts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'lang'], 'string', 'max' => 110],
            [['text'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'text' => 'Text',
            'lang' => 'Lang',
        ];
    }
}
