<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\bootstrap\ActiveForm;
use yii\web\UploadedFile;

$this->title = 'Додати дані';
?>
<h1>Додати</h1>
<div class="line content-right">
</div>
<div class="content-left">
	<?php $form = ActiveForm::begin([
        'id' => 'ClientsDataForm-form',
        'layout' => 'horizontal',
        'options' => [
        	'enctype' => 'multipart/form-data',
        ],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-4\">{input}</div>\n<div class=\"col-lg-4\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-4 control-label'],
        ],
    ]); ?>
        <?= $form->field($model, 'name')->textInput(['autofocus' => true, 'value' => isset($clientData->name) ? $clientData->name : ''])->label('Ім\'я') ?>
        <?= $form->field($model, 'surname')->textInput(['value' => isset($clientData->surname) ? $clientData->surname : ''])->label('Прізвище') ?>

        <?= $form->field($model, 'phone')->textInput(['value' => isset($clientData->phone) ? $clientData->phone : ''])->label('Телефон') ?>
        <?= $form->field($model, 'email')->textInput(['value' => isset($clientData->email) ? $clientData->email : ''])->label('E-mail') ?>
        <div class="form-group">
            <div class="col-lg-offset-5 col-lg-7">
                <?= Html::submitButton($submitName, ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
        </div>

    <?php ActiveForm::end(); ?>
</div>