<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\bootstrap\ActiveForm;
use yii\web\UploadedFile;

$this->title = 'Додати дані';
?>
<h1>Додати</h1>
<div class="line content-right">
</div>
<div class="content-left">
	<?php $form = ActiveForm::begin([
        'id' => 'ProductsCategoriesDataForm-form',
        'layout' => 'horizontal',
        'options' => [
        	'enctype' => 'multipart/form-data',
        ],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-4\">{input}</div>\n<div class=\"col-lg-4\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-4 control-label'],
        ],
    ]); ?>
        <?= $form->field($model, 'name')->textInput(['autofocus' => true, 'value' => isset($categoryData->name) ? $categoryData->name : ''])->label('Назва категорії') ?>

		<?php if(isset($categoryData->image)) { ?>
			<div class="form-group field-products-name has-success">
				<label class="col-lg-4 control-label">Фото</label>
				<div class="col-lg-4 queryImage">
					<img src="img/products/<?=$categoryData->image?>" alt="">
				</div>
				<div class="col-lg-4"></div>
			</div>
			<?= $form->field($model, 'image')->fileInput()->label('Замінити фото') ?>
		<?php } else { ?>
        <?= $form->field($model, 'image')->fileInput()->label('Фото') ?>
		<?php } ?>

        <?= $form->field($model, 'active')->checkbox([
            'template' => "<div class=\"col-lg-offset-4 col-lg-4\">{input} {label}</div>\n<div class=\"col-lg-4\">{error}</div>",
            'checked ' => $value = (isset($categoryData->active) && $categoryData->active == 1) ? true : false ,
        ])->label('Активний') ?>

        <div class="form-group">
            <div class="col-lg-offset-5 col-lg-7">
                <?= Html::submitButton($submitName, ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
        </div>

    <?php ActiveForm::end(); ?>
</div>