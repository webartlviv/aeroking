<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\bootstrap\ActiveForm;
use yii\web\UploadedFile;

$this->title = 'Додати дані';
?>
<h1>Додати</h1>
<div class="line content-right">
</div>
<div class="content-left">
	<?php $form = ActiveForm::begin([
        'id' => 'RequestsDataForm-form',
        'layout' => 'horizontal',
        'options' => [
        	'enctype' => 'multipart/form-data',
        ],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-4\">{input}</div>\n<div class=\"col-lg-4\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-4 control-label'],
        ],
    ]); ?>
        <?= $form->field($model, 'name')->textInput(['autofocus' => true, 'value' => isset($requestData->name) ? $requestData->name : ''])->label('Ім\'я') ?>

        <?= $form->field($model, 'phone')->textInput(['value' => isset($requestData->phone) ? $requestData->phone : ''])->label('Телефон') ?>
        <?= $form->field($model, 'email')->textInput(['value' => isset($requestData->email) ? $requestData->email : ''])->label('E-mail') ?>
        <?php  $param = isset($requestData->status) ? ['options' =>[ $requestData->status => ['Selected' => true]]] : array() ; ?>

        <?= $form->field($model, 'status')->dropDownList([
            '1' => 'Новий',
            '2' => 'Опрацьовано',
            '3' => 'Виконано'
        ] , $param )->label('Соц. мережа') ?>

        <?= $form->field($model, 'delivery')->textarea(['value' => isset($requestData->delivery) ? $requestData->delivery : ''])->label('Адреса доставки') ?>
        <?= $form->field($model, 'comment')->textarea(['value' => isset($requestData->comment) ? $requestData->comment : ''])->label('Коментар') ?>

        <?php if(isset($requestData->order)) {
                $orders = json_decode($requestData->order, true);
                ?>
                <span class="divTableProductsHeader">Замовлення</span>
                <div class="divTableProducts">
                    <div class="header">
                        <div class="cell">Назва</div>
                        <div class="cell">Кількість</div>
                        <div class="cell">Ціна</div>
                        <div class="cell">Наповнення</div>
                        <div class="cell">Колір</div>
                        <div class="cell">Сума</div>
                    </div>
                    <?php foreach ($orders as $order): ?>
                        <div class="line">
                            <div class="cell"><?= $order['Name'] ?></div>
                            <div class="cell"><?= $order['Count'] ?></div>
                            <div class="cell"><?= $order['Price'] ?></div>
                            <div class="cell"><?= $order['Napovn'] ?></div>
                            <div class="cell"><?= $order['Color'] ?></div>
                            <div class="cell"><?= $order['Summ'] ?></div>
                        </div>
                    <?php endforeach; ?>
                </div>
        <?php  } ?>
        <div class="form-group">
            <div class="col-lg-offset-5 col-lg-7">
                <?= Html::submitButton($submitName, ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
        </div>

    <?php ActiveForm::end(); ?>
</div>