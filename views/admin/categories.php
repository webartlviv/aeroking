<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
?>
<h1>Усі товари</h1>
<div class="line content-right">
	<button class="btn add"><span class="glyphicon glyphicon-plus"></span><label class="categories">Додати категорію</label></button>
</div>
<div class="divTable">
	<div class="header">
		<div class="cell">ID</div>
		<div class="cell">Зображення</div>
		<div class="cell">Назва</div>
	</div>
<?php foreach ($categories as $category): ?>
    <div class="line">
    	<div class="cell id"><span class="categories"><?= $category->id ?></span></div>
		<div class="cell">
			<?php if($category->image) { ?>
			<img src="img/categories/<?= $category->image ?>">
			<?php } else { ?>
				немає зображення
			<?php } ?>
		</div>
		<div class="cell"><?= Html::encode("{$category->name}") ?></div>
    </div>
<?php endforeach; ?>
</div>

<?= LinkPager::widget(['pagination' => $pagination]) ?>