<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
?>
<h1>Покупці</h1>
<div class="line content-right">
	<button class="btn add"><span class="glyphicon glyphicon-plus"></span><label class="clients">Додати</label></button>
</div>
<div class="divTable">
	<div class="header">
		<div class="cell">ID</div>
		<div class="cell">Ім'я</div>
		<div class="cell">Прізвище</div>
		<div class="cell">Телефон</div>
		<div class="cell">E-mail</div>
	</div>
<?php foreach ($clients as $client): ?>
    <div class="line">
    	<div class="cell id"><span class="clients"><?= $client->id ?></span></div>
		<div class="cell"><?= Html::encode("{$client->name}") ?></div>
		<div class="cell"><?= Html::encode("{$client->surname}") ?></div>
		<div class="cell"><?= $client->phone ?></div>
		<div class="cell"><?= $client->email ?></div>
    </div>
<?php endforeach; ?>
</div>

<?= LinkPager::widget(['pagination' => $pagination]) ?>