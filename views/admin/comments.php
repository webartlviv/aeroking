<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
?>
<h1>Коментарі</h1>
<div class="line content-right">
	<button class="btn add"><span class="glyphicon glyphicon-plus"></span><label class="comments">Додати</label></button>
</div>
<div class="divTable .comments">
	<div class="header">
		<div class="cell">ID</div>
		<div class="cell">Ім'я</div>
		<div class="cell">Коментар</div>
		<div class="cell">Картинка</div>
		<div class="cell">Джерело</div>
	</div>
<?php foreach ($comments as $comment): ?>
    <div class="line">
    	<div class="cell id"><span class="comments"><?= $comment->id ?></span></div>
		<div class="cell"><?= Html::encode("{$comment->name}") ?></div>
		<div class="cell"><?= Html::encode("{$comment->comment}") ?></div>
		<div class="cell"><img src="img/comments/<?= $comment->image ?>"></div>
		<div class="cell">
			<?php if($comment->from) { 
				switch ($comment->from) {
					case 'vk':
						$name = 'Вконтакте';
						$url = 'http://vk.com';
						break;
					
					case 'fb':
						$name = 'Facebook';
						$url = 'http://fb.com';
						break;

					case 'ins':
						$name = 'Instagram';
						$url = 'http://instagram.com';
						break;
				} ?>
				<a href="<?= $url ?>"><?= $name ?></a>
			<?php } ?>
		</div>
    </div>
<?php endforeach; ?>
</div>

<?= LinkPager::widget(['pagination' => $pagination]) ?>