<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
?>
<h1>Усі товари</h1>
<div class="line content-right">
	<button class="btn add"><span class="glyphicon glyphicon-plus"></span><label class="categories">Додати категорію</label></button>
	<button class="btn add"><span class="glyphicon glyphicon-plus"></span><label class="products">Додати товар</label></button>
</div>
<div class="divTable">
	<div class="header">
		<div class="cell">ID</div>
		<div class="cell">Категорія</div>
		<div class="cell">Зображення</div>
		<div class="cell">Назва</div>
		<div class="cell">Опис</div>
		<div class="cell">Ціна</div>
	</div>
<?php foreach ($products as $product): ?>
    <div class="line">
    	<div class="cell id"><span class="products"><?= $product->id ?></span></div>
		<div class="cell"><?= Html::encode("{$categoryName[$product->category_id]}") ?></div>
		<div class="cell">
			<?php if($product->image) { ?>
			<img src="img/products/<?= $product->image ?>">
			<?php } else { ?>
				немає зображення
			<?php } ?>
		</div>
		<div class="cell"><?= Html::encode("{$product->name}") ?></div>
		<div class="cell"><?= $product->description ?></div>
		<div class="cell"><?= $product->price ?></div>
    </div>
<?php endforeach; ?>
</div>

<?= LinkPager::widget(['pagination' => $pagination]) ?>