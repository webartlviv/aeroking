<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
?>
<h1>Заявки</h1>
<div class="line content-right">
	<button class="btn add"><span class="glyphicon glyphicon-plus"></span><label class="requests">Додати</label></button>
</div>
<div class="divTable">
	<div class="header">
		<div class="cell">ID</div>
		<div class="cell">Ім'я</div>
		<div class="cell">Телефон</div>
		<div class="cell">E-mail</div>
		<div class="cell">Статус</div>
		<div class="cell">Дата</div>
		<div class="cell">Коментар</div>
	</div>
<?php foreach ($requests as $request): ?>
    <div class="line">
    	<div class="cell id"><span class="requests"><?= $request->id ?></span></div>
		<div class="cell"><?= Html::encode("{$request->name}") ?></div>
		<div class="cell"><?= $request->phone ?></div>
		<div class="cell"><?= $request->email ?></div>
		<div class="cell"><?= $request->status ?></div>
		<div class="cell"><?= date('d-m-Y H:i:s', strtotime($request->date) ) ?></div>
		<div class="cell"><?= $request->comment ?></div>
    </div>
<?php endforeach; ?>
</div>

<?= LinkPager::widget(['pagination' => $pagination]) ?>