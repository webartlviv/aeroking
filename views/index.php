<?php include('head.php'); ?>
</head>
<body>

	<!-- Здесь пишем код -->
<!-- Текст-рыба -->
<!-- <img class="nullBg" src="img/bg01.jpg"> -->
<section class="menu">
	<div class="title">
		<a href="#section_one"><span>главная</span></a>
		<a href="#section_sixth"><span>игрушки</span></a>
		<a href="#section_seventh"><span>о нас</span></a>
        <a href="#section_tenth"><span>отзывы</span></a>
        <a href="#section_twelfth"><span>контакты</span></a>
        <button>заказать magic globe</button>
	</div>
</section>
<section class="row">
	<a href="" id="after_arr_down">
		<div class="guide">
			<img src="img/row/download.png" id="after_arr_down_img">
		</div>
		<div class="nextSlide">
			<span class="slideText">следующий слайд:</span>
			<span class="variableSlideText" id="next_slide_text"></span>
		</div>
	</a>
	<a href="#section_one" id="after_arr_up">
		<div class="guide">
			<img src="img/row/download.png" id="after_arr_down_img">
		</div>
		<div class="nextSlide">
			<span class="slideText">Посмотреть еще раз</span>
		</div>
	</a>
	<div class="order">
		<button>заказать magic globe</button>
	</div>
	<div class="rowText">
		<span>Поделитесь с друзьями и мы подарим скидку
        10%, на Вашу первую покупку <span class="globe">magic globe</span></span>
	</div>
	<div class="social">
		<div class="img">
			<a href="#"><img src="img/row/vk.png"></a>
		</div>
		<div class="img">
			<a href="#"><img src="img/row/fb.png"></a>
		</div>
		<div class="img">
			<a href="#"><img src="img/row/tv.png"></a>
		</div>
		<div class="img">
			<a href="#"><img src="img/row/od.png"></a>
		</div>
	</div>
</section>
<section class="one" id="section_one">
	<div class="bg">
		<img src="img/one/background1.png">
	</div>
	<div class="content">
		<div class="container">
			<div class="leftLogo">
				<img src="img/one/logo.png">
			</div>
			<div class="planet">
			    <div class="imgPlanet">
			        <img src="img/one/planet.png">
			    </div>
				<div class="oneAkcio">
					<div class="akcioBg">
						<img src="img/one/action2.png">
					</div>
					<div class="oneTextAkcio">
						<span class="textAkcio">Успей</span>
						<span class="textAkcio">получить</span>
						<span class="textAkcio">скидку</span>
						<span class="bigTextAkcio">30%</span>
						<span class="textAkcio">до <span class="numericAkcio">20</span></span>
						<span class="monthAkcio">октебря</span>
					</div>
				</div>
			</div>
			<div class="call">
				<span class="info">Звонки по России бесплатни</span>
				<span class="tellOne">8(800)-775-93-18</span>
				<button class="oneOrderCall">заказать звонок</button>
			</div>
			<div class="oneDeliveri">
			    <span class="oneTextDeliveri">доставка<br>при оплате онлайн - <br>бесплатно</span>
			</div>
			<div class="oneRightFuter">
				<div class="whot">
					<div class="textWhot">
						<span>Что такое<br>magic globe?</span>
					</div>
					<div class="imgWhot">
						<img src="img/one/play.png">
					</div>
				</div>
				<div class="awards">
					<div class="imgAwards">
						<img src="img/one/awards1.png">
					</div>
					<div class="imgAwards">
						<img src="img/one/awards2.png">
					</div>
					<div class="imgAwards">
						<img src="img/one/awards3.png">
					</div>
					<div class="imgAwards">
						<img src="img/one/awards4.png">
					</div>
					<div class="imgAwards">
						<img src="img/one/awards5.png">
					</div>
				</div>
				<div class="oneTextPisces">
					<span class="textPisces">Текст рыбы условий акции</span>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="second" id="section_second">
	<div class="bg">
		<img src="img/second/bg1.png">
	</div>
	<div class="content">
	    <div class="title">
	    	<h6>польза детям , отдых родителям !</h6>
	    </div>
		<div class="container" id="secondBlockContainer">
			<div class="block">
				<a href="#section_five">
					<div class="secondBlockImg">
						<img src="img/second/favor_1.jpg">
					</div>
					<div class="secondBlockText">
						<span class="secondBlockDescription">изобретателям</span>
					</div>
				</a>
			</div>
			<div class="block">
				<a href="#section_five">
					<div class="secondBlockImg">
						<img src="img/second/favor_2.jpg">
					</div>
					<div class="secondBlockText">
						<span class="secondBlockDescription">фантазерам</span>
					</div>
				</a>
			</div>
			<div class="block">
				<a href="#section_five">
					<div class="secondBlockImg">
						<img src="img/second/favor_3.jpg">
					</div>
					<div class="secondBlockText">
						<span class="secondBlockDescription">непоседам</span>
					</div>
				</a>
			</div>
			<div class="block">
				<a href="#section_five">
					<div class="secondBlockImg">
						<img src="img/second/favor_4.jpg">
					</div>
					<div class="secondBlockText">
						<span class="secondBlockDescription">мечтателям</span>
					</div>
				</a>
			</div>
			<div class="block">
				<a href="#section_five">
					<div class="secondBlockImg">
						<img src="img/second/favor_5.jpg">
					</div>
					<div class="secondBlockText">
						<span class="secondBlockDescription">совсем маленьким</span>
					</div>
				</a>
			</div>
			<div class="block">
				<a href="#section_five">
					<div class="secondBlockImg">
						<img src="img/second/favor_6.jpg">
					</div>
					<div class="secondBlockText">
						<span class="secondBlockDescription">капризулям</span>
					</div>
				</a>
			</div>
			<div class="block">
				<a href="#section_five">
					<div class="secondBlockImg">
						<img src="img/second/favor_7.jpg">
					</div>
					<div class="secondBlockText">
						<span class="secondBlockDescription">волшебникам</span>
					</div>
				</a>
			</div>
			<div class="block">
				<a href="#section_five">
					<div class="secondBlockImg">
						<img src="img/second/favor_8.jpg">
					</div>
					<div class="secondBlockText">
						<span class="secondBlockDescription">энштейнам</span>
					</div>
				</a>
			</div>
		</div>
		<div class="warning">
			<span>внимание! дети становятся послушными!!!</span>
		</div>
	</div>
</section>
<section class="third" id="section_third">
	<div class="bg">
		<img src="img/third/bg.png">
	</div>
	<div class="content">
		<div class="thirdLeftFoto">
			<img src="img/third/headleft.png">
		</div>
		<div class="thirdTitle">
			<h6>счастье, запечатленное на фото</h6>
			<span class="textThirdTitle">Поделитесь с друзьями и мы подарим скидку 10%, на Вашу первую покупку<span class="textThirdTitle_Globe">MAGIC GLOBE</span></span>
			<div class="insThirdTitle">
				<div class="imgInsThirdTitle">
					<img src="img/third/ins.png">
				</div>
				<div class="textInsThirdTitle">
					<a href="#">#magic_globe</a>
				</div>
			</div>
		</div>
		<div class="thirdRightFoto">
			<img src="img/third/headright.png">
		</div>
		<div class="container" id="bb-custom-grid">
			<div class="thirdBloc">
	            <div class="thirdBlocSlide">
    	            <div class="filling">
    	                <div class="thirdBlocSlideImg">
    	    	            <img src="img/third/one.jpg">
    	                </div>
    	                <div class="scrolling">
    	           	        <span class="bb_current"></span>
    	           	        <span></span>
    	           	        <span></span>
    	                </div>
    	            </div>
	    	    </div>
	    	    <div class="text">
	    		   	<span>Поделитесь с друзьями и мы подарим скидку 10%, на Вашу первую покупку <span class="globe">magic-gears</span></span>
	    		</div>
    		</div>
    		<div class="thirdBloc">
	            <div class="thirdBlocSlide">
    	            <div class="filling">
    	                <div class="thirdBlocSlideImg">
    	    	            <img src="img/third/two.jpg">
    	                </div>
    	                <div class="scrolling">
    	           	        <span class="bb_current"></span>
    	           	        <span></span>
    	           	        <span></span>
    	                </div>
    	            </div>
	    	    </div>
	    	    <div class="text">
	    		   	<span>Поделитесь с друзьями и мы подарим скидку 10%, на Вашу первую покупку <span class="globe">magic-gears</span></span>
	    		</div>
    		</div>
    		<div class="thirdBloc">
	            <div class="thirdBlocSlide">
    	            <div class="filling">
    	                <div class="thirdBlocSlideImg">
    	    	            <img src="img/third/two.jpg">
    	                </div>
    	                <div class="scrolling">
    	           	        <span class="bb_current"></span>
    	           	        <span></span>
    	           	        <span></span>
    	                </div>
    	            </div>
	    	    </div>
	    	    <div class="text">
	    		   	<span>Поделитесь с друзьями и мы подарим скидку 10%, на Вашу первую покупку <span class="globe">magic-gears</span></span>
	    		</div>
    		</div>
    		<div class="thirdBloc">
	            <div class="thirdBlocSlide">
    	            <div class="filling">
    	                <div class="thirdBlocSlideImg">
    	    	            <img src="img/third/three.jpg">
    	                </div>
    	                <div class="scrolling">
    	           	        <span class="bb_current"></span>
    	           	        <span></span>
    	           	        <span></span>
    	                </div>
    	            </div>
	    	    </div>
	    	    <div class="text">
	    		   	<span>Поделитесь с друзьями и мы подарим скидку 10%, на Вашу первую покупку <span class="globe">magic-gears</span></span>
	    		</div>
    		</div>
    	</div>  
    	<div class="thirdBottomtFoto">
    		<img src="img/third/bottom.png">
    	</div>  
	</div>
</section>
<section class="fourth" id="section_fourth">
	<div class="bg">
		<img src="img/fourth/bg.png">
	</div>
	<div class="bg1">
		<img src="img/fourth/family.png">
	</div>
	<div class="content">
	    <div class="fourthTitle">
	    	<h6>фантазируйте, путишествуйте и играйте!</h6>
	    </div>
		<div class="container">
			<div class="fourthLeftTop">
				<span class="fourthText">Зая в игровой форме развивает у ребенка любознательность, кругозор, словарный запас, связную и грамотную речь, внимание и анализ</span>
			</div>
			<div class="fourthRightTop">
				<span class="fourthText">Каждая Зая - это целых четыре игрушки в одной. Просто переверните игрушку спинкой или вверх лапами. С каждой стороны вы увидите новую Заю в новом платье</span>
			</div>
			<div class="fourthLeftBottom">
				<span class="fourthText">Игрушка-перевертыш дает неограниченные возможности для игры, развития фантазии и креатива. Одна игрушка - миллион сюжетов.
                </span>
			</div>
			<div class="fourthRightBottom">
				<span class="fourthText">С каждой стороны Заи мордочки с разными эмоциями, что помогает малышу моделировать полную гамму эмоциональных переживаний</span>
			</div>
		</div>
	</div>
</section>
<section class="five" id="section_five">
	<div class="bg">
		<img src="img/five/bg.png">
	</div>
	<div class="content">
		<div class="leftPlanet">
			<img src="img/five/left_planet.png">
		</div>
		<div class="fiveTitle">
			<h6>счасте,<br> запечатлееное на видео</h6>
			<div class="fiveTitleText">
			    <span class="fiveText">Наш видео канал на Ютюбе</span>
		    </div>
		    <div class="fiveTitlePlanet">
			    <img src="img/five/center_planet.png">
		    </div>
		</div>
		<div class="rightPhoto">
			<img src="img/five/right_photos.png">
		</div>
		<div class="container">
			<div class="leftPhoto">
				<img src="img/five/left_photos.png">
			</div>
			<div class="fiveBlock">
				<div class="porthole">
					<div class="center_img">
		        		<div class="previev">
		        			<img src="img/five/cover1.png" alt="">
		        		</div>
		        		<img class="bgFivePorthole" src="img/five/share.png">
						<div class="play">
							<img src="img/five/play.png" alt="">
						</div>
		        	</div>
		        	<div class="textFivePorthole">
		        		<span>Поделитесь с друзьями и мы подарим скидку 10%, на Вашу первую покупку</span>
		        	</div>
				</div>
				<div class="porthole">
					<div class="center_img">
		        		<div class="previev">
		        			<img src="img/five/cover2.jpg" alt="">
		        		</div>
		        		<img class="bgFivePorthole" src="img/five/share.png">
						<div class="play">
							<img src="img/five/play.png" alt="">
						</div>
		        	</div>
		        	<div class="textFivePorthole">
		        		<span>Поделитесь с друзьями и мы подарим скидку 10%, на Вашу первую покупку</span>
		        	</div>
				</div>
				<div class="porthole">
					<div class="center_img">
		        		<div class="previev">
		        			<img src="img/five/cover3.png" alt="">
		        		</div>
		        		<img class="bgFivePorthole" src="img/five/share.png">
						<div class="play">
							<img src="img/five/play.png" alt="">
						</div>
		        	</div>
		        	<div class="textFivePorthole">
		        		<span>Поделитесь с друзьями и мы подарим скидку 10%, на Вашу первую покупку</span>
		        	</div>
				</div>
			</div>
			<div class="rightPlanet">
				<img src="img/five/right_planet.png">
			</div>
		</div>
	</div>
</section>
<section class="sixth" id="section_sixth">
	<div class="bg">
		<img src="img/sixth/bg.png">
	</div>
	<div class="content">
		<div class="sixthTitle">
			<h6>magic globe</h6>
			<div class="sixthTitleYuoTube">
				<div class="imgYuoTube">
					<img src="img/sixth/better.png">
				</div>
				<div class="textYouTube">
					<span class="contextYouTube">Лучше один раз увидеть!</span>
				</div>
			</div>
			<div class="container">
			    <div class="sixtBlockhGlobe">
					<div class="ice">
						<div class="globeTitle">
							<span>ice</span>
						</div>
						<div class="globeImg">
							<img src="img/sixth/ice.png">
						</div>
					</div>
					<div class="water">
						<div class="globeTitle">
							<span>water</span>
						</div>
						<div class="globeImg">
						    <div class="globeWaterImg">
								<img src="img/sixth/water.png">
							</div>
							<div class="globeHit">
								<img src="img/sixth/hit.png">
							</div>
						</div>
					</div>
					<div class="stone">
						<div class="globeTitle">
							<span>stone</span>
						</div>
						<div class="globeImg">
							<img src="img/sixth/stone.png">
						</div>
					</div>
					<div class="wing">
						<div class="globeTitle">
							<span class="wing">wing</span>
						</div>
						<div class="globeImg">
							<img src="img/sixth/wing.png">
						</div>
					</div>
				</div>	
				<div class="sixtBlockhAkce">
					<div class="textSixthChoice">
						<span>Выберите понравившиеся magic globe и отправьте нам заявку</span>
					</div>
				    <div class="sixthAkce">
						<div class="sixthTextPrice">
							<span class="price">1500 руб.</span>
						</div>
						<div class="formSixthAkce">
							<div class="bgForm">
								<img src="img/sixth/action.png">
							</div>
							<div class="textSixthAkce">
								<span class="textSixthAkceTitle">Акция</span>
								<span class="textSixthAkceDescription">Только сегодня шары по влекательной цене!</span>
							</div>
							<form>
								<input class="textSixthForm" type="text" name="text" placeholder="Ваше имя и фамилия">
								<input class="textSixthForm" type="tel" name="tel" placeholder="Ваш телефон">
								<button class="orderSixthForm">получить скидку</button>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="textSixthFotter">
				<span class="descriptionSixthFotter">Выберите понравившиеся magic globe и отправьте нам заявку</span>
			</div>
		</div>
	</div>
</section>
<section class="seventh" id="section_seventh">
	<div class="bg">
		<img src="img/seventh/bg.png">
	</div>
	<div class="content">
		<div class="title">
			<h6>несколько причин почему magic globe так популярны!</h6>
		</div>
		<div class="seventhContainer">
			<div class="causes">
				<div class="seventhBlock">
					<div class="seventhBlockImg">
						<img src="img/seventh/certificate.png">
					</div>
					<div class="seventhBlockText">
						<span>они крутыэ! подтверждено сертификатами</span>
					</div>
				</div>
				<div class="seventhBlock">
					<div class="seventhBlockImg">
						<img src="img/seventh/time.png">
					</div>
					<div class="seventhBlockText">
						<span>больше свободного времени родителям!</span>
					</div>
				</div>
				<div class="seventhBlock">
					<div class="seventhBlockImg">
						<img src="img/seventh/children.png">
					</div>
					<div class="seventhBlockText">
						<span>для дитей от 1 годика</span>
					</div>
				</div>
				<div class="seventhBlock">
					<div class="seventhBlockImg">
						<img src="img/seventh/delivery.png">
					</div>
					<div class="seventhBlockText">
						<span>быстрая доставка по россии, сутки по питеру</span>
					</div>
				</div>
				<div class="seventhBlock">
					<div class="seventhBlockImg">
						<img src="img/seventh/awards.png">
					</div>
					<div class="seventhBlockText">
						<span>нашы гарантии</span>
					</div>
				</div>
				<div class="seventhBlock">
					<div class="seventhBlockImg">
						<img src="img/seventh/kopilka.png">
					</div>
					<div class="seventhBlockText">
						<span>реальная цена</span>
					</div>
				</div>
			</div>
			<div class="seventhPlanet">
				<img src="img/seventh/planet.png">
			</div>
		</div>
	</div>
</section>
<section class="eighth" id="section_eighth">
	<div class="content">
		<div class="title">
			<h6>инструкцыя для получения magoc globe</h6>
		</div>
		<div class="container">
			<div class="eighthBlock">
				<div class="eighthBlockImg">
					<img src="img/eighth/order.png">
				</div>
				<div class="eighthBlockImgText">
					<span>делайте заказ у нас на сайте</span>
				</div>
			</div>
			<div class="eighthBlock">
				<div class="eighthBlockImg">
					<img src="img/eighth/manager.png">
				</div>
				<div class="eighthBlockImgText">
					<span>наш менеджер по телефону уточняет у вас адрес доставки</span>
				</div>
			</div>
			<div class="eighthBlock">
				<div class="eighthBlockImg">
					<img src="img/eighth/delivery.png">
				</div>
				<div class="eighthBlockImgText">
					<span>служба доставки спешит с ценним грузом к вам</span>
				</div>
			</div>
			<div class="eighthBlock">
				<div class="eighthBlockImg">
					<img src="img/eighth/get.png">
				</div>
				<div class="eighthBlockImgText">
					<span>ви получаете отличною игру и оплачиваете ее</span>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="nine" id="section_nine">
	<div class="bg">
		<img src="img/nine/bg.png">
	</div>
	<div class="content">
		<div class="title">
			<h6>посмотрим видео с картинками разказами об етих удивительных magic globe</h6>
		</div>
		<div class="container">
			<div class="videoBlock">
		 	    <iframe src="https://www.youtube.com/embed/VahRdazaKnQ" frameborder="0" allowfullscreen></iframe>
		    </div>
		</div>
	</div>
</section>
<section class="tenth" id="section_tenth">
	<div class="bg">
		<img src="img/tenth/bg.png">
	</div>
	<div class="content">
		<div class="title">
			<h6>почитаите отзывы счасливых обладателей magic globe</h6>
		</div>
		<div class="container">
			<div class="tenthBlock">
				<div class="tenthBlockTitle">
					<div class="tenthBlockTitleImg">
						<img src="img/tenth/one.png">
					</div>
					<div class="tenthBlockTitleName">
						<span class="tenthBlockTitleNameText">Ирина Москва</span>
					</div>
				</div>
				 <div class="tenthBlockContent">
				 	<div class="tenthBlockContentTitle">
				 		<span class="tenthBlockContentTitle1">«Простой чизкейк»</span>
				 	</div>
				 	<div class="tenthBlockContentText">
				 		<span class="tenthBlockContentText1">Торт я сразу ждала в предвкушении необычайного украшения праздника и, признаюсь, не ошиблась! На детском празднике всегда много сладкоежек</span>
				 	</div>
				 </div>
			</div>
			<div class="tenthBlock">
				<div class="tenthBlockTitle">
					<div class="tenthBlockTitleImg">
						<img src="img/tenth/two.png">
					</div>
					<div class="tenthBlockTitleName">
						<span class="tenthBlockTitleNameText">Ирина Москва</span>
					</div>
				</div>
				 <div class="tenthBlockContent">
				 	<div class="tenthBlockContentTitle">
				 		<span class="tenthBlockContentTitle1">«по-новому!»</span>
				 	</div>
				 	<div class="tenthBlockContentText">
				 		<span class="tenthBlockContentText1">Торт я сразу ждала в предвкушении необычайного украшения праздника и, признаюсь, не ошиблась! Торт я сразу ждала в предвкушении необычайного украшения праздника и, признаюсь, не ошиблась!Ирина, </span>
				 	</div>
				 </div>
			</div>
			<div class="tenthBlock">
				<div class="tenthBlockTitle">
					<div class="tenthBlockTitleImg">
						<img src="img/tenth/three.png">
					</div>
					<div class="tenthBlockTitleName">
						<span class="tenthBlockTitleNameText">Ирина Москва</span>
					</div>
				</div>
				 <div class="tenthBlockContent">
				 	<div class="tenthBlockContentTitle">
				 		<span class="tenthBlockContentTitle1">«Простой по-новому!»</span>
				 	</div>
				 	<div class="tenthBlockContentText">
				 		<span class="tenthBlockContentText1">Ирина, Москва Простой чизкейк - по-новому!»«Торт я сразу ждала в предвкушении необычайного украшения праздника и, признаюсь, не ошиблась!Ирина, </span>
				 	</div>
				 </div>
			</div>
			<div class="tenthBlock">
				<div class="tenthBlockTitle">
					<div class="tenthBlockTitleImg">
						<img src="img/tenth/four.png">
					</div>
					<div class="tenthBlockTitleName">
						<span class="tenthBlockTitleNameText">Ирина Москва</span>
					</div>
				</div>
				 <div class="tenthBlockContent">
				 	<div class="tenthBlockContentTitle">
				 		<span class="tenthBlockContentTitle1">«чизкейк - по-новому!»</span>
				 	</div>
				 	<div class="tenthBlockContentText">
				 		<span class="tenthBlockContentText1">Торт я сразу ждала в предвкушении необычайного украшения праздника и, признаюсь, не ошиблась! Ирина, Москва Простой чизкейк - по-новому!»«Торт я сразу ждала в предвкушении необычайного украшения праздника и, признаюсь, не ошиблась!Ирина, </span>
				 	</div>
				 </div>
			</div>
		</div>
		<div class="container next_reviews">
			<div class="tenthBlock">
				<div class="tenthBlockTitle">
					<div class="tenthBlockTitleImg">
						<img src="img/tenth/one.png">
					</div>
					<div class="tenthBlockTitleName">
						<span class="tenthBlockTitleNameText">Ирина Москва</span>
					</div>
				</div>
				 <div class="tenthBlockContent">
				 	<div class="tenthBlockContentTitle">
				 		<span class="tenthBlockContentTitle1">«Простой чизкейк»</span>
				 	</div>
				 	<div class="tenthBlockContentText">
				 		<span class="tenthBlockContentText1">Торт я сразу ждала в предвкушении необычайного украшения праздника и, признаюсь, не ошиблась! На детском празднике всегда много сладкоежек</span>
				 	</div>
				 </div>
			</div>
			<div class="tenthBlock">
				<div class="tenthBlockTitle">
					<div class="tenthBlockTitleImg">
						<img src="img/tenth/two.png">
					</div>
					<div class="tenthBlockTitleName">
						<span class="tenthBlockTitleNameText">Ирина Москва</span>
					</div>
				</div>
				 <div class="tenthBlockContent">
				 	<div class="tenthBlockContentTitle">
				 		<span class="tenthBlockContentTitle1">«по-новому!»</span>
				 	</div>
				 	<div class="tenthBlockContentText">
				 		<span class="tenthBlockContentText1">Торт я сразу ждала в предвкушении необычайного украшения праздника и, признаюсь, не ошиблась! Торт я сразу ждала в предвкушении необычайного украшения праздника и, признаюсь, не ошиблась!Ирина, </span>
				 	</div>
				 </div>
			</div>
			<div class="tenthBlock">
				<div class="tenthBlockTitle">
					<div class="tenthBlockTitleImg">
						<img src="img/tenth/three.png">
					</div>
					<div class="tenthBlockTitleName">
						<span class="tenthBlockTitleNameText">Ирина Москва</span>
					</div>
				</div>
				 <div class="tenthBlockContent">
				 	<div class="tenthBlockContentTitle">
				 		<span class="tenthBlockContentTitle1">«Простой по-новому!»</span>
				 	</div>
				 	<div class="tenthBlockContentText">
				 		<span class="tenthBlockContentText1">Ирина, Москва Простой чизкейк - по-новому!»«Торт я сразу ждала в предвкушении необычайного украшения праздника и, признаюсь, не ошиблась!Ирина, </span>
				 	</div>
				 </div>
			</div>
			<div class="tenthBlock">
				<div class="tenthBlockTitle">
					<div class="tenthBlockTitleImg">
						<img src="img/tenth/four.png">
					</div>
					<div class="tenthBlockTitleName">
						<span class="tenthBlockTitleNameText">Ирина Москва</span>
					</div>
				</div>
				 <div class="tenthBlockContent">
				 	<div class="tenthBlockContentTitle">
				 		<span class="tenthBlockContentTitle1">«чизкейк - по-новому!»</span>
				 	</div>
				 	<div class="tenthBlockContentText">
				 		<span class="tenthBlockContentText1">Торт я сразу ждала в предвкушении необычайного украшения праздника и, признаюсь, не ошиблась! Ирина, Москва Простой чизкейк - по-новому!»«Торт я сразу ждала в предвкушении необычайного украшения праздника и, признаюсь, не ошиблась!Ирина, </span>
				 	</div>
				 </div>
			</div>
		</div>
		<div class="tenthFooter">
			<span class="tenthFooterText" id="tenthFooterText">еще отзывы:</span>
		</div>
	</div>
</section>
<section class="eleventh" id="section_eleventh">
	<div class="bg">
		<img src="img/eleventh/bg.png">
	</div>
	<div class="content">
		<div class="container">
			<div class="formEleventh">
			    <span class="eleventhText title">нашы контакты:</span>
			    <span class="eleventhText adress">наш магази находитса по адресу:</span>
			    <span class="eleventhText adressA">санкт-петербург.</span>
			    <span class="eleventhText adressB">ул. Гожуленская. Д. 26</span>
			    <span class="eleventhText emailTitle">e-mail</span>
			    <span class="emailText">ADVAITAPR@ГИРС.COM</span>
			    <span class="eleventhText time">часы роботы:</span>
			    <span class="eleventhText timeText">10:00 - 20:00</span>
			    <button class="feedback">обратная связь</button>
			</div>
		</div>
	</div>
</section> 
<section class="twelfth" id="section_twelfth">
	<div class="content">
		<div class="container">
			<div class="blockInfo">
				<span class="titleBlockInfo">Информацыя о компании:</span>
				<span class="textBlockInfo">Оплата и доставка</span>
				<span class="textBlockInfo">Для-партнеров</span>
				<span class="textBlockInfo">Товары оптом под заказ</span>
				<span class="textBlockInfo">Правовая информация</span>
				<span class="textBlockInfo">Гарантия безопасности</span>
				<span class="textBlockInfo">Договор оферты</span>
			</div>
			<div class="blockContacts">
				<div class="blockContactsImg">
					<img src="img/twelfth/skype.png">
				</div>
				<div class="twelfthcontact">
					<span class="titleTwelfthcontact">Контакты:</span>
					<span class="textTwelfthcontact">+7 (800) 000-00-00 (бесплатно)</span>
					<span class="textTwelfthcontact">+7 (000) 000-00-00 (оптовый отдел)</span>
					<span class="textTwelfthcontact">гирс@mail.ru</span>
					<span class="textTwelfthcontact">гирс@mail.ru</span>
				</div>
			</div>
			<div class="blockSoc">
				<span class="titleABlockSoc">Мы в соц. сетях:</span>
				<div class="imgBlockSoc">
					<div class="imgSoc">
						<img src="img/twelfth/vk.png">
					</div>
					<div class="imgSoc">
						<img src="img/twelfth/fb.png">
					</div>
					<div class="imgSoc">
						<img src="img/twelfth/tw.png">
					</div>
					<div class="imgSoc">
						<img src="img/twelfth/od.png">
					</div>
				</div>
				<div class="detailsBlockSoc">
					<span class="titledetailsBlockSoc">Наши реквизиты:</span>
				</div>
			</div>	
			<div class="blockYuoTube">
				<img src="img/twelfth/youtube.png">
			</div>
		</div>
	</div>
</section>

	<div class="hidden"></div>

	<!-- Конец кода страници -->

	<!-- Подгрузка скриптов -->
	<!-- Тут прописывать нужные скрипты  -->
<!--[if lt IE 9]>
	<script src="libs/html5shiv/es5-shim.min.js"></script>
	<script src="libs/html5shiv/html5shiv.min.js"></script>
	<script src="libs/html5shiv/html5shiv-printshiv.min.js"></script>
	<script src="libs/respond/respond.min.js"></script>
	<![endif]-->

	<script>
		function loadCSS(hf) {
			var ms=document.createElement("link");ms.rel="stylesheet";
			ms.href=hf;document.getElementsByTagName("head")[0].appendChild(ms);
		}
		// loadCSS("libs/animate/animate.css"); //Загрузка библиотеки CSS: Animate CSS
		// loadCSS("https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css");     //Стили bootstrap
		// loadCSS("https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css");     //Стили bootstrap
		// loadCSS("js/owlcarousel/owl.carousel.min.css");              //Стили owlcarousel
		// loadCSS("js/owlcarousel/owl.theme.default.min.css");         //Стили owlcarousel
		loadCSS("css/_fonts.css");              //Шрифты
		// loadCSS("css/_header.css");              //Стандартные стили (скомилировать!)
		loadCSS("css/_style.css");               //Стили страници (perfectPixel при макетном разрищении)
		loadCSS("css/_responsive.css");          //Стили адаптации 
	</script>

	<!-- Load CSS compiled without Bootstrap & Header styles (послерелизная загрузка) -->
	<script>var ms=document.createElement("link");ms.rel="stylesheet";
		// ms.href="compiled.min.css";document.getElementsByTagName("head")[0].appendChild(ms);
	</script>

	<!-- Load Scripts -->
	<script>var scr = {"scripts":[
		{"src" : "libs/modernizr/modernizr.js", "async" : false},
		{"src" : "libs/jquery/jquery-1.11.2.min.js", "async" : false},
		{"src" : "libs/waypoints/waypoints.min.js", "async" : false},
		{"src" : "libs/animate/animate-css.js", "async" : false},
		{"src" : "libs/plugins-scroll/plugins-scroll.js", "async" : false},
		{"src" : "js/bookblock.min.js", "async" : false},
		{"src" : "js/jquery.bookblock.min.js", "async" : false},
		{"src" : "ljs/modernizr.custom.js", "async" : false},
		// {"src" : "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js", "async" : false},
		// {"src" : "js/owlcarousel/owl.carousel.min.js", "async" : false},
		{"src" : "js/common.js", "async" : false}
		]};!function(t,n,r){"use strict";var c=function(t){if("[object Array]"!==Object.prototype.toString.call(t))return!1;for(var r=0;r<t.length;r++){var c=n.createElement("script"),e=t[r];c.src=e.src,c.async=e.async,n.body.appendChild(c)}return!0};t.addEventListener?t.addEventListener("load",function(){c(r.scripts);},!1):t.attachEvent?t.attachEvent("onload",function(){c(r.scripts)}):t.onload=function(){c(r.scripts)}}(window,document,scr);
	</script>
<?php include('counters.php'); ?>
</body>
</html>