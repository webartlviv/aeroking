<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="shortcut icon" href="img/favicon.png">
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
    <!-- <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
    </div> -->
<div class="wrap">
<?php
    if (!Yii::$app->user->isGuest) {
        NavBar::begin([
            'brandLabel' => 'AeroKing',
            'brandUrl' => Yii::$app->homeUrl,
            'options' => [
                'class' => 'navbar-inverse navbar-fixed-top',
            ],
        ]);
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => [
                ['label' => 'Адмінка', 'url' => ['/admin/']],
                // ['label' => 'Налаштування', 'url' => ['/admin/settings']],
                ['label' => 'Заявки', 'url' => ['/admin/requests']],
                ['label' => 'Категорії товарів', 'url' => ['/admin/categories']],
                ['label' => 'Товари', 'url' => ['/admin/products']],
                ['label' => 'Покупці', 'url' => ['admin/clients']],
                ['label' => 'Відгуки', 'url' => ['/admin/comments']],
                Yii::$app->user->isGuest ? (
                    ['label' => 'Вхід', 'url' => ['/admin/login']]
                ) : (
                    '<li>'
                    . Html::beginForm(['/site/logout'], 'post')
                    . Html::submitButton(
                        'Вихід (' . Yii::$app->user->identity->username . ')',
                        ['class' => 'btn btn-link logout']
                    )
                    . Html::endForm()
                    . '</li>'
                )
            ],
        ]);
        NavBar::end();
    }
    ?>
    <div class="container"><?= $content ?></div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Webart <?= date('Y') ?></p>

        <p class="pull-right"><a href="http://webart-lviv.com.ua">Студія розробки та дизайну WebArt</a></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
