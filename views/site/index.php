<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
use yii\helpers\Html;

$templateName = Yii::$app->params['activeLayout'];

include "{$templateName}_index.php";

?>
<!-- Подгрузка скриптов -->
<!--[if lt IE 9]>
	<script src="libs/html5shiv/es5-shim.min.js"></script>
	<script src="libs/html5shiv/html5shiv.min.js"></script>
	<script src="libs/html5shiv/html5shiv-printshiv.min.js"></script>
	<script src="libs/respond/respond.min.js"></script>
	<![endif]-->

	<script>
		function loadCSS(hf) {
			var ms=document.createElement("link");ms.rel="stylesheet";
			ms.href=hf;document.getElementsByTagName("head")[0].appendChild(ms);
		}
		loadCSS("css/templates/<?= $templateName ?>/_fonts.css");
		loadCSS("css/templates/<?= $templateName ?>/_header.css");
		loadCSS("css/templates/<?= $templateName ?>/_style.css");
		loadCSS("css/templates/<?= $templateName ?>/_responsive.css");
		loadCSS("css/templates/<?= $templateName ?>/<?= $templateName ?>.css");
	</script>

	<!-- Load Scripts -->
	<script>var scr = {"scripts":[
		{"src" : "js/common.js", "async" : false}
		]};!function(t,n,r){"use strict";var c=function(t){if("[object Array]"!==Object.prototype.toString.call(t))return!1;for(var r=0;r<t.length;r++){var c=n.createElement("script"),e=t[r];c.src=e.src,c.async=e.async,n.body.appendChild(c)}return!0};t.addEventListener?t.addEventListener("load",function(){c(r.scripts);},!1):t.attachEvent?t.attachEvent("onload",function(){c(r.scripts)}):t.onload=function(){c(r.scripts)}}(window,document,scr);
	</script>

       
<!-- Тут прописывать нужные скрипты  -->
<div class="hidden"></div>