<?php

use yii\helpers\Html;
?>
<section class="fifth">
    <div class="content">
        <div class="information">
            <span class="info_text fifthTextColor"><?= Html::encode("{$langText['information']}") ?></span>
            <div class="policy" data-toggle="modal" data-target="#modalPolicy">
                <span class="policy_text fifthTextColor"><?= Html::encode("{$langText['Privacypolicy']}") ?></span>
            </div>
        </div>
        <div class="contacts fifthTextColor">
            <span class="title"><?= Html::encode("{$langText['OurContects']}") ?></span>
            <span class="text"><?= Html::encode("{$langText['OurAddress']}") ?></span><br>
            <span class="title"><?= Html::encode("{$langText['WriteUs']}") ?></span>
            <span class="text">info@aeroking.com.ua</span>
            <span class="title"><?= Html::encode("{$langText['callUs']}") ?></span>
            <a href="tel:0800753073" class="text">0800 753 073</a>
            <a href="tel:0932595588" class="text">(093) 259 55 88</a>
            <a href="tel:‎0680595588" class="text">‎(068) 059 55 88</a>
        </div>
        <div class="soc">
            <span class="title_soc fifthTextColor"><?= Html::encode("{$langText['weInsoc']}") ?>:</span>
            <div class="img_soc">
                <a href="https://www.facebook.com/groups/215705865570088/">
                    <div class="img">
                        <img src="img/minimum/fifth/fb.png">
                    </div>
                </a>
                <a href="https://vk.com/aeroking_com_ua">    
                    <div class="img">
                        <img src="img/minimum/fifth/vk.png">
                    </div>
                </a>
                <a href="https://www.instagram.com/aeroking.com.ua/">    
                    <div class="img">
                        <img src="img/minimum/fifth/ins.png">
                    </div>
                </a>
                <a href="https://www.youtube.com/channel/UCC2vCp5lEJh7h64AMcQThtw">    
                    <div class="img">
                        <img src="img/minimum/fifth/you.png">
                    </div>
                </a>    
            </div>
        </div>
    </div>
</section>