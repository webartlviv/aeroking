<?php

use yii\helpers\Html;
?>
<section class="first" id="sectionGeneral">
    <div class="bg">
        <div class="carrousel_first owl-carousel">
            <div class="item">
                <img src="img/minimum/first/bg.jpg">
            </div>
            <div class="item">
                <img src="img/minimum/first/bg2.jpg">
            </div>
            <div class="item">
                <img src="img/minimum/first/bg3.jpg">
            </div>
            <div class="item">
                <img src="img/minimum/first/bg4.jpg">
            </div>
        </div>
    </div>
    <div class="content">
        <div class="previous" id="owl-prev">
            <div class="img">
                <img src="img/minimum/first/previous.png"">
            </div>
        </div>
        <div class="center">
            <div class="logo">
                <img src="img/minimum/first/logo1.png">
            </div>
            <div class="text_logo">
                <span class="slogan"><?= Html::encode("{$langText['slogan']}") ?></span>
            </div>
        </div>
        <div class="following" id="owl-next">
            <div class="img">
                <img src="img/minimum/first/following.png">
            </div>
        </div>
        <div class="delivery">
            <div class="img">
                <img src="img/minimum/first/delivery.png">
            </div>
            <div class="text pulse">
                <span class="text_delivery firstTextColor"><?= Html::encode("{$langText['fastDelivery']}") ?></span>
            </div>
        </div>
        <!-- <div class="action">
                 <div class="img">
                    <a href="#sectionAction">
                        <img src="img/minimum/first/action.png">
                    </a>
                </div>
                <a href="#sectionAction" class="text pulse">
                    <span class="text_action firstTextColor"><?= Html::encode("{$langText['timetoget']}") ?><br><?= Html::encode("{$langText['discount']}") ?> <?=$discount ?><br><?= Html::encode("{$langText['to']}") ?> <?= $discountDate ?></span>
                </a>
        </div> -->
    </div>
</section>