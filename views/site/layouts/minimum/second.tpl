<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$model = new \app\models\ContactForm;
?>
<section class="second" id="sectionAction">
    <div class="bg">
        <img src="img/minimum/second/bg.png">
    </div>  
        <div class="content">
            <div class="title">
                <h3><?= Html::encode("{$langText['uniquePropositions']}") ?></h3>
            </div>
            <div class="container">
                <div class="block">
                    <div class="previous" id="second-owl-prev">
                        <img src="img/minimum/second/previous.png">
                    </div>
                    <div class="carrousel_second owl-carousel" id="secondProductsBlock">
                        <?php foreach ($uniqueProducts as $product) { ?>
                        <div class="item">
                            <div class="img">
                                    <img class="img_bg" src="img/minimum/second/portrait.png">
                                    <div class="goods">
                                        <?php $image = (isset($product->image)) ? $product->image : 'product_no_img.jpg' ; ?>
                                        <img src="img/products/<?= $image ?>">
                                    </div>  
                            </div>
                            <div class="information">
                                <div class="name">
                                    <span class="title_name_text secondCarrouselTextColor"><?= Html::encode("{$product->name}") ?></span>
                                    <span class="name_text thirdCarrousel_nemeTextTextColor none"><?= Html::encode("{$product->description}") ?></span>
                                </div>
                                <div class="price">
                                    <span class="old_price thirdOldCarrouselTextColor none"><?=  round($product->price * 100 / 70) ?> грн. за <?= Html::encode("{$units[$product->unit]['sort_name']}") ?></span>
                                    <span class="price_text secondCarrouselTextColor"><span><?= Html::encode("{$product->price}") ?></span> грн. за <?= Html::encode("{$units[$product->unit]['sort_name']}") ?></span>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="following" id="second-owl-next">
                        <img src="img/minimum/second/following.png">
                    </div>
                </div>
                <div class="discount">
                    <div class="conditions">
                        <div class="bg_conditions">
                            <img src="img/minimum/second/action.png">
                        </div>
                        <div class="form_conditions">
                            <div class="text">
                                <span class="conditions_text secondConditionsTextColor"><br><br><br>0800 753 073<br><a href="tel:0800753073"><?= Html::encode("{$langText['menuAboutPhone']}") ?></a><br>lifecell: (093) 259 55 88<br>‎kyivstar: (068) 059 55 88</span>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
</section>