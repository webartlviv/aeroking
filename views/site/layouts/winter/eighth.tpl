<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$model = new \app\models\ContactForm;
?>
<section class="eighth">
    <div class="bg">
        <img src="img/winter/eighth/bg.png">
    </div>
        <div class="content">
            <div class="title">
                <h3><?= Html::encode("{$langText['havequestions']}") ?>?</h3>
            </div>
            <div class="form">
                <div class="form_text">
                    <span class="eighthTextColor"><?= $langText['havequestionsText'] ?></span>
                </div>
                <?php $form = ActiveForm::begin([
                    'id' => 'ContactForm-form',
                    'layout' => 'horizontal',
                    'fieldConfig' => [
                        'template' => "{input}",
                        'inputOptions' => ['class' => 'eighthFormTextColor'], 
                    ],
                ]); ?>
                    <?= $form->field($model, 'name')->textInput(['autofocus' => true, 'placeholder' => "{$langText['name']}", 'class' => "name"]) ?>
                    <?= $form->field($model, 'phone')->textInput(['placeholder' => Html::encode("{$langText['phone']}"), 'class' => "tel"]) ?>
                    <?= Html::submitButton(Html::encode("{$langText['callme']}"), ['class' => 'order_eighth eighthOrderTextColor pulseButton', 'name' => 'login-button']) ?>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
</section>