<?php

use yii\helpers\Html;
?>
<section class="fifth" id="sectionAbout">
    <div class="bg">
        <img src="img/winter/fifth/bg.png">
    </div>
    <div class="content">
        <div class="advantages">
            <div class="img">
                <img src="img/winter/fifth/fast_delivery.png">
            </div>
            <div class="text">
                <span class="fifthTextColor"><?= $langText['FastdeliverytoUkraine'] ?></span>
            </div>
        </div>
        <div class="advantages">
            <div class="img">
                <img src="img/winter/fifth/promotions.png">
            </div>
            <div class="text">
                <span class="fifthTextColor"><?= $langText['Recentpromotionaloffers'] ?></span>
            </div>
        </div>
        <div class="advantages">
            <div class="img">
                <img src="img/winter/fifth/proven_quality.png">
            </div>
            <div class="text">
                <span class="fifthTextColor"><?= $langText['Onlycheckedandqualityproduct'] ?></span>
            </div>
        </div>
        <div class="advantages">
            <div class="img">
                <img src="img/winter/fifth/reception.png">
            </div>
            <div class="text">
                <span class="fifthTextColor"><?= $langText['Goodservice'] ?></span>
            </div>
        </div>
        <div class="advantages">
            <div class="img">
                <img src="img/winter/fifth/holiday_atmosphere.png">
            </div>
            <div class="text">
                <span class="fifthTextColor"><?= $langText['Guaranteedfestiveatmosphere'] ?></span>
            </div>
        </div>
        <div class="advantages">
            <div class="img">
                <img src="img/winter/fifth/real_price.png">
            </div>
            <div class="text">
                <span class="fifthTextColor"><?= $langText['Realprice'] ?></span>
            </div>
        </div>
    </div>
</section>