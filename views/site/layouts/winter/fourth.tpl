<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$model = new \app\models\BuyForm;
?>
<section class="fourth" id="selected_products">
    <div class="bg">
        <img src="img/winter/fourth/bg.png">
    </div>
    <div class="bgMob">
        <div class="bgMob0">
            <img src="img/winter/fourth/bg_mob.jpg">
        </div>
        <div class="bgMob1">
            <img src="img/winter/fourth/bg_line.png">
        </div>
    </div>
    <div class="bgMob2">
            <img src="img/winter/fourth/bg_line.png">
    </div>
    <div class="content">
        <div class="title">
            <h3><?= Html::encode("{$langText['ordering']}") ?></h3>
        </div>
        <div class="container">
            <div class="logo_left">
                <div class="img">
                    <img src="img/winter/fourth/logo_order.png">
                    <div class="logo_img">
                        <img id="buyProductImg" src="img/winter/fourth/no_tovar.jpg">
                    </div>
                </div>
                <div class="logo_text">
                    <div class="name">
                        <span class="name_text fourthCarrousel_nemeTextColor" id="buyProductName"></span><br>
                        <span class="title_name_text thirdCarrousel_nemeTextTextColor" id="buyProductDescription"></span>
                    </div>
                    <div class="price">
                        <span class="old_price thirdOldCarrouselTextColor" id="buyProductOldPrice"></span><br>
                        <span class="price_text fourthCarrouselTextColor" id="buyProductPrice"></span>
                    </div>
                </div>
            </div>
            <div class="ordering">
                <div class="bg_ordering">
                    <img src="img/winter/fourth/bg_order.png">
                </div>
                <div class="ordering_name">
                    <?php $form = ActiveForm::begin([
                        'id' => 'BuyForm-form',
                        'layout' => 'horizontal',
                        'fieldConfig' => [
                            'template' => "{input} {label}",
                            'inputOptions' => ['class' => 'filling fourthFormColor'],
                            'labelOptions' => ['class' => 'control-label']
                        ],
                    ]); ?>
                        <?= $form->field($model, 'name')->textInput(['autofocus' => true, 'placeholder' => "{$langText['name']}", 'class' => 'form_name'])->label('') ?>
                        <?= $form->field($model, 'phone')->textInput(['placeholder' => Html::encode("{$langText['phone']}"), 'class' => 'form_tel'])->label('') ?>
                        <div class="info_select_color">
                            <div class="color">
                                <ul>
                                    <li class="header" onclick="header_isNap()">
                                        <span class="mingle fourthFormColor" id="is_selectNap_0">Наповнено гелієм</span>
                                        <div class="img_batton">
                                            <img src="img/winter/fourth/batton.png">
                                        </div>
                                    </li>
                                    <div id="is_select_hidden_blockNap" onclick="header_is()" style="display: none;"></div>
                                    <div class="is_select_block fourthFormColor" id="is_select_blockNap" style="display: none;">
                                        <li id="is_select_blockNap_0" onclick="select_isNap(0)">Наповнено гелієм</li>
                                        <li id="is_select_blockNap_1" onclick="select_isNap(1)">Наповнено повітрям</li>
                                    </div>
                                </ul>
                            </div>
                        </div>  
                        <div class="info_select_color">
                            <div class="color">
                                <ul>
                                    <li class="header" onclick="header_is()">
                                        <span class="mingle fourthFormColor" id="is_select_0">мішані кольори</span>
                                        <div class="img_batton">
                                            <img src="img/winter/fourth/batton.png">
                                        </div>
                                    </li>
                                    <div id="is_select_hidden_block" onclick="header_is()" style="display: none;"></div>
                                    <div class="is_select_block fourthFormColor" id="is_select_block" style="display: none;">
                                        <?php foreach ($colors as $color) { ?>
                                        <li id="is_select_<?= $color->id ?>" onclick="select_is(<?= $color->id ?>)"><?= $color->name ?></li>
                                        <?php } ?>
                                    </div>
                                </ul>
                            </div>
                            <div class="img_what" data-toggle="modal" data-target="#modalColors">
                                <img src="img/winter/fourth/what.png">
                            </div>
                        </div>
                        <?= $form->field($model, 'product')->textInput(['id' => 'buyFormProduct'])->label('') ?>
                        <?= $form->field($model, 'price')->textInput(['id' => 'buyFormPrice'])->label('') ?>
                        <?= $form->field($model, 'napovn')->textInput(['id' => 'buyFormNapovn'])->label('') ?>
                        <?= $form->field($model, 'color')->textInput(['id' => 'buyFormColor'])->label('') ?>

                        <?= $form->field($model, 'count')->textInput(['placeholder' => "1", 'value' => '1', 'class' => 'number', 'type' => 'number', 'id' => 'orderCount'])->label("<span class='number_piecesmingle fourthForm_textColor'>штук</span>") ?>
                        <div class="amount">
                            <span class="text_amount fourthForm_textColor"><?= Html::encode("{$langText['summ']}") ?>: </span>
                            <span class="numeric_amount fourthForm_textColor" id="orederSumm"></span>
                            <span class="grn_amount fourthForm_textColor"> грн </span>
                        </div>
                        <?= Html::submitButton(Html::encode("{$langText['orderBuy']}"), ['class' => 'order_fourth thirdOrder_priceTextColor pulseButton', 'name' => 'login-button']) ?>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
            <div class="text_right">
                <div class="text_rightImg">
                    <img src="img/winter/fourth/girl.png">
                </div>
                <div class="work_schedule">
                    <span class="consultant fourthText_rightColor"><?= $langText['notstandardorder'] ?><br><a data-toggle="modal" data-target="#modalCall"><?= Html::encode("{$langText['contactourconsultant']}") ?></a><br><?= $langText['andagreealldetails'] ?></span>
                    <div class="time fourthText_rightColor"><div><span><?= Html::encode("{$langText['receivingorders']}") ?></span></div><div><span><?= Html::encode("{$langText['worktime']}") ?></span></div><div><span><?= Html::encode("{$langText['daily']}") ?></span></div></div>
                </div>
            </div>
        </div>
    </div>
</section>