<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$model = new \app\models\ContactForm;
?>
<div class="pop_up_window" id="alertBlock">
    <span class="pop_up_window_text" id="alertBlockText"><?= $messageAlert ?></span>
    <span class="glyphicon glyphicon-remove" id="alertBlockClose"></span>
</div>
<div class="modal fade modalConditions" id="modalCall" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modalCallContent">
            <div class="close" id="modalCallClose">
                    <img src="img/cloze/cross.png">
            </div>
            <div class="bg_conditions">
                <img src="img/winter/second/action.png">
            </div>
            <div class="form_conditions">
                <div class="text">
                    <span class="conditions_text secondConditionsTextColor"><?= $langText['modalCall_header'] ?></span>
                </div>
                <?php $form = ActiveForm::begin([
                    'id' => 'ContactForm-form',
                    'layout' => 'horizontal',
                    'fieldConfig' => [
                        'template' => "{input}",
                        'inputOptions' => ['class' => 'filling secondFillingTextColor'], 
                    ],
                ]); ?>
                    <?= $form->field($model, 'name')->textInput(['autofocus' => true, 'placeholder' => "{$langText['name']}"]) ?>
                    <?= $form->field($model, 'phone')->textInput(['placeholder' => Html::encode("{$langText['phone']}")]) ?>
                    <?= Html::submitButton(Html::encode("{$langText['orderBuy']}"), ['class' => 'order_conditions secondConditionsOrderTextColor pulseButton', 'name' => 'login-button']) ?>
                <?php ActiveForm::end(); ?>
                <div class="end">
                    <span><?= $langText['modalCall_grafic'] ?></span>
                </div>
            </div>
        </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade modalConditions" id="modalColors" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modalColorsContent">
            <div class="close_one" id="modalColorsClose">
                <a href="#close">
                    <img src="img/cloze/cross1.png">
                </a>
            </div>
            <?php foreach ($colors as $color) { ?>
            <div class="model_balls">
                <div class="img_model">
                    <img src="img/modalColors/<?= $color->image ?>">
                </div>
                <div class="text_model">
                    <span class="number"><?= $color->id ?></span>
                    <span class="palette"><?= $color->name ?></span>
                </div>
            </div>
            <?php } ?>
        </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade modalConditions" id="modalPolicy" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modalPolicyContent">
            <div class="close_thwo" id="modalPolicyClose">
                <div class="titleModalPolicy">
                    <span><?= Html::encode("{$langText['confidentialPolicyHeader']}") ?></span>
                </div>
                <a href="#close">
                    <img src="img/cloze/cross2.png">
                </a>
            </div>
            <div class="textModalPolicy">
                <span class="descriptionModalPolicy">
                    <?= $langText['confidentialPolicy'] ?>
                </span>
            </div>
        </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<section class="menu">
    <div class="content">
        <div class="title">
            <span class="text menuTextColor"><a href="#sectionGeneral"><?= Html::encode("{$langText['menuGeneral']}") ?></a></span>
        </div>
        <div class="title">
            <span class="text menuTextColor"><a href="#sectionProduction"><?= Html::encode("{$langText['menuProduction']}") ?></a></span>
        </div>
        <div class="title">
            <span class="text menuTextColor"><a href="#sectionAbout"><?= Html::encode("{$langText['menuAbout']}") ?></a></span>
        </div>
        <div class="title">
            <span class="text menuTextColor"><a href="#sectionReviews"><?= Html::encode("{$langText['menuReviews']}") ?></a></span>
        </div>
        <div class="fone">
            <span class="tel menuTextColor"><a href="tel:0800753073">0800 753 073</a></span>
            <span class="text_fone menuTextColor"><?= Html::encode("{$langText['menuAboutPhone']}") ?></span>
        </div>
        <div class="buttom">
            <button class="order_menu" data-toggle="modal" data-target="#modalCall"><?= Html::encode("{$langText['modalCallButton']}") ?></button>
        </div>
    </div>  
</section>