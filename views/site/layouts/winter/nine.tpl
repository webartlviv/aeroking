<?php

use yii\helpers\Html;
?>
<section class="nine">
    <div class="content">
        <div class="information">
            <span class="info_text nineTextColor"><?= Html::encode("{$langText['information']}") ?>інформація</span>
            <div class="policy" data-toggle="modal" data-target="#modalPolicy">
                <span class="policy_text nineTextColor"><?= Html::encode("{$langText['Privacypolicy']}") ?></span>
            </div>
        </div>
        <div class="contacts nineTextColor">
            <span class="title"><?= Html::encode("{$langText['OurContects']}") ?></span>
            <span class="text"><?= Html::encode("{$langText['OurAddress']}") ?></span><br>
            <span class="title"><?= Html::encode("{$langText['WriteUs']}") ?></span>
            <span class="text">info@aeroking.com.ua</span>
            <span class="title"><?= Html::encode("{$langText['callUs']}") ?></span>
            <a href="tel:0800753073" class="text">0800 753 073</a>
        </div>
        <div class="soc">
            <span class="title_soc nineTextColor"><?= Html::encode("{$langText['weInsoc']}") ?>:</span>
            <div class="img_soc">
                <a href="https://www.facebook.com/groups/215705865570088/">
                    <div class="img">
                        <img src="img/minimum/fifth/fb.png">
                    </div>
                </a>
                <a href="https://vk.com/aeroking_com_ua">    
                    <div class="img">
                        <img src="img/minimum/fifth/vk.png">
                    </div>
                </a>
                <a href="https://www.instagram.com/aeroking.com.ua/">    
                    <div class="img">
                        <img src="img/minimum/fifth/ins.png">
                    </div>
                </a>
                <a href="https://www.youtube.com/channel/UCC2vCp5lEJh7h64AMcQThtw">    
                    <div class="img">
                        <img src="img/minimum/fifth/you.png">
                    </div>
                </a>    
            </div>
        </div>
    </div>
</section>