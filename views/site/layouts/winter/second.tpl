<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$model = new \app\models\ContactForm;
?>
<section class="second" id="sectionAction">
    <div class="bg">
        <img src="img/winter/second/bg.png">
    </div>  
        <div class="content">
            <div class="title">
                <h3><?= Html::encode("{$langText['uniquePropositions']}") ?></h3>
            </div>
            <div class="container">
                <div class="block">
                    <div class="previous" id="second-owl-prev">
                        <img src="img/winter/second/previous.png">
                    </div>
                    <div class="carrousel_second owl-carousel" id="secondProductsBlock">
                        <?php foreach ($uniqueProducts as $product) { ?>
                        <div class="item">
                            <div class="img">
                                    <img class="img_bg" src="img/winter/second/portrait.png">
                                    <div class="goods">
                                        <?php $image = (isset($product->image)) ? $product->image : 'product_no_img.jpg' ; ?>
                                        <img src="img/products/<?= $image ?>">
                                    </div>  
                            </div>
                            <div class="information">
                                <div class="name">
                                    <span class="title_name_text secondCarrouselTextColor"><?= Html::encode("{$product->name}") ?></span>
                                    <span class="name_text thirdCarrousel_nemeTextTextColor none"><?= Html::encode("{$product->description}") ?></span>
                                </div>
                                <div class="price">
                                    <span class="old_price thirdOldCarrouselTextColor none"><?=  round($product->price * 100 / 70) ?> грн. за <?= Html::encode("{$units[$product->unit]['sort_name']}") ?></span>
                                    <span class="price_text secondCarrouselTextColor"><span><?= Html::encode("{$product->price}") ?></span> грн. за <?= Html::encode("{$units[$product->unit]['sort_name']}") ?></span>
                                </div>
                                <div class="order">
                                    <button class="order_price secondOrder_priceTextColor pulseButton"><?= Html::encode("{$langText['orderBuy']}") ?></button>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="following" id="second-owl-next">
                        <img src="img/winter/second/following.png">
                    </div>
                </div>
                <div class="discount">
                    <div class="action">
                        <span class="action_text secondActionTextColor"><?=$discount ?></span>
                    </div>
                    <div class="conditions">
                        <div class="bg_conditions">
                            <img src="img/winter/second/action.png">
                        </div>
                        <div class="form_conditions">
                            <div class="text">
                                <span class="conditions_text secondConditionsTextColor"><?= Html::encode("{$langText['available']}") ?><br><?= Html::encode("{$langText['discountisvalid']}") ?><br><?= Html::encode("{$langText['onlyto']}") ?><br><?= $discountDate ?><br><?= Html::encode("{$langText['timeto']}") ?><br><?= Html::encode("{$langText['ordernow']}") ?></span>
                            </div>
                            <?php $form = ActiveForm::begin([
                                'id' => 'ContactForm-form',
                                'layout' => 'horizontal',
                                'fieldConfig' => [
                                    'template' => "{input}",
                                    'inputOptions' => ['class' => 'filling secondFillingTextColor'], 
                                ],
                            ]); ?>
                                <?= $form->field($model, 'name')->textInput(['autofocus' => true, 'placeholder' => "{$langText['name']}"]) ?>
                                <?= $form->field($model, 'phone')->textInput(['placeholder' => Html::encode("{$langText['phone']}")]) ?>
                                <?= Html::submitButton(Html::encode("{$langText['orderBuy']}"), ['class' => 'order_conditions secondConditionsOrderTextColor pulseButton', 'name' => 'login-button']) ?>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
</section>