<?php

use yii\helpers\Html;
?>
<section class="seventh" id="sectionReviews">
    <div class="bg">
        <img src="img/winter/seventh/mascot.png">
    </div>
    <div class="bg_one">
        <img src="img/winter/seventh/bg_thwo.png">
    </div>
    <div class="content">
        <div class="title">
            <h3><?= Html::encode("{$langText['Testimonials']}") ?></h3>
        </div>
        <div class="carrousel_seventh owl-carousel">
            <?php 
            $ancor = $ancor != 'left' ? 'left' : 'right';
            foreach ($comments as $comment) { ?>
            <div class="item <?= $ancor ?>">
                <div class="item_block">
                    <div class="img">
                        <img src="img/winter/seventh/left_bord.png">
                        <div class="img_bg">
                            <img src="img/comments/<?= $comment->image; ?>">
                        </div>
                    </div>
                    <div class="text">
                        <span class="seventhTextColor"><?= Html::encode("{$comment->comment}") ?></span>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</section>