<?php

use yii\helpers\Html;
?>
<section class="sixth">
    <div class="bg">
        <img src="img/winter/sixth/bg.png">
    </div>
    <div class="content">
        <div class="title">
            <h3><?= Html::encode("{$langText['4stepstoholidays']}") ?></h3>
        </div>
        <div class="container">
            <div class="design">
                <div class="img_substitute">
                    <img src="img/winter/sixth/bg_img.png">
                    <div class="bg_img">
                        <img src="img/winter/sixth/design.jpg">
                    </div>
                </div>
                <div class="text">
                    <span class="sixthTextColor"><?= $langText['executionorder'] ?></span>
                </div>
            </div>
            <div class="confirmation">
                <div class="img_substitute">
                    <img src="img/winter/sixth/bg_img.png">
                    <div class="bg_img">
                        <img src="img/winter/sixth/confirmation.jpg">
                    </div>
                </div>
                <div class="text">
                    <span class="sixthTextColor"><?= $langText['confirmation'] ?></span>
                </div>
            </div>
            <div class="delivery">
                <div class="img_substitute">
                    <img src="img/winter/sixth/bg_img.png">
                    <div class="bg_img">
                        <img src="img/winter/sixth/delivery.jpg">
                    </div>
                </div>
                <div class="text">
                    <span class="sixthTextColor"><?= $langText['delivery'] ?></span>
                </div>
            </div>
            <div class="holiday">
                <div class="img_substitute">
                    <img src="img/winter/sixth/bg_img.png">
                    <div class="bg_img">
                        <img src="img/winter/sixth/holiday.jpg">
                    </div>
                </div>
                <div class="text">
                    <span class="sixthTextColor"><?= $langText['holiday'] ?>    </span>
                </div>
            </div>
        </div>
    </div>
</section>