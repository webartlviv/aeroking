<?php

use yii\helpers\Html;
?>
<section class="third" id="sectionProduction">
    <div class="bg">
        <img src="img/winter/third/bg_line.png">
        <div class="fon">
            <img src="img/winter/third/bg1.png">
        </div>
    </div>
    <div class="content">
        <div class="title">
            <h3><?= Html::encode("{$langText['ourproducts']}") ?></h3>
        </div>
        <div class="container">
            <div class="menuBlock">
                <?php foreach ($categories as $category) { ?>
                <div class="menu" id="productionsMenu_<?= $category->id ?>" onclick="changeCarosuel(<?= $category->id ?>)">
                    <div class="bg_menu">
                        <img src="img/winter/third/bg_menu.png">
                    </div>
                    <div class="img">
                        <img src="img/winter/third/img_menu.png">
                        <div class="bg_img_menu">
                            <?php $image = (isset($product->image)) ? $product->image : 'product_no_img.jpg' ; ?>
                            <img src="img/categories/<?= $category->image ?>">
                        </div>
                    </div>
                    <div class="text_menu">
                        <h6><?= $category->name ?></h6>
                    </div>
                </div>
                <?php } ?>
            </div>
            <div class="block" id="productsBlock">
                <div class="previous" id="third-owl-prev">
                    <img src="img/winter/third/previous.png">
                </div>
                    <?php foreach ($products as $categoryId => $categoryProducts) { 
                        if(isset($categoryName[$categoryId])) { ?>
                        <div class="carrousel_third owl-carousel" id="carrousel_third_<?= $categoryId ?>">
                        <?php foreach ($categoryProducts as $key => $product) {?>
                            <div class="item">
                                <div class="img_carrousel">
                                        <img class="img_bg_carrousel" src="img/winter/third/img_karusel.png">
                                        <div class="goods">
                                            <img src="img/products/<?= $product->image ?>">
                                        </div>  
                                </div>
                                <div class="information">
                                    <div class="name">
                                        <span class="title_name_text thirdCarrousel_nemeTextColor"><?= Html::encode("{$product->name}") ?></span>
                                        <span class="name_text thirdCarrousel_nemeTextTextColor"><?= Html::encode("{$product->description}") ?></span>
                                    </div>
                                    <div class="price">
                                        <span class="old_price thirdOldCarrouselTextColor"><?=  round($product->price * 100 / 70) ?> грн. за <?= Html::encode("{$units[$product->unit]['sort_name']}") ?></span>
                                        <span class="price_text thirdCarrouselTextColor"><span><?= Html::encode("{$product->price}") ?></span> грн. за <?= Html::encode("{$units[$product->unit]['sort_name']}") ?></span>
                                    </div>
                                    <div class="order">
                                        <button class="order_price thirdOrder_priceTextColor pulseButton"><?= Html::encode("{$langText['orderBuy']}") ?></button>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        </div>
                    <?php } } ?>
                <div class="following" id="third-owl-next">
                    <img src="img/winter/third/following.png">
                </div>  
            </div>  
        </div>
    </div>
</section>