$(window).scrollTop(0);

$(document).ready(function() {
	jQuery("a[href*=\\#]").click(function () {
		var elementClick = $(this).attr("href")
		var destination = $(elementClick).offset().top;
		jQuery("html:not(:animated),body:not(:animated)").animate({scrollTop: destination}, 800);
		return false;
	});
});
jQuery(function($){
	$("input[name*=\\phone]").mask("+38(099)999-99-99");
});

$('#modalCall').on('shown.bs.modal', function () {
  $('#modalCall').focus()
})

$('#modalCallClose').click(function () {
  $('#modalCall').modal('hide');
})

$('#modalColors').on('shown.bs.modal', function () {
  $('#modalColors').focus()
})

$('#modalColorsClose').click(function () {
  $('#modalColors').modal('hide');
})

$('#modalPolicy').on('shown.bs.modal', function () {
  $('#modalPolicy').focus()
})

$('#modalPolicyClose').click(function () {
  $('#modalPolicy').modal('hide');
})

$(document).ready(function(){
  	var owl = $(".carrousel_first");
    owl.owlCarousel({
        loop:true,
	    margin:0,
	    nav:false,
	    items: 1,
	    autoplay:true,
	    autoplayTimeout: 4000
    });

    $("#owl-next").click(function(){
	  owl.trigger('next.owl');
	});
	$("#owl-prev").click(function(){
	  owl.trigger('prev.owl');
	});
});

$(document).ready(function(){
  	var owl = $(".carrousel_second");
    owl.owlCarousel({
        loop:true,
	    margin:0,
	    nav:false,
	    items: 3,
	    autoplay:true,
	    autoplayTimeout: 5000,
	    autoplayHoverPause: true
    });

    $("#second-owl-next").click(function(){
	  owl.trigger('next.owl');
	});
	$("#second-owl-prev").click(function(){
	  owl.trigger('prev.owl');
	});
});

$(document).ready(function(){
	$('.menu:first-child').addClass('active');
	$('.carrousel_third').first().css('display', 'flex');

  	var owl = $(".carrousel_third").first();
    owl.owlCarousel({
        loop:true,
	    margin:0,
	    nav:false,
	    items: 4,
	    autoplay:true,
	    autoplayTimeout: 5000,
	    autoplayHoverPause: true
    });

    $("#third-owl-next").click(function(){
	  owl.trigger('next.owl');
	});
	$("#third-owl-prev").click(function(){
	  owl.trigger('prev.owl');
	});
});

$(document).ready(function(){
  	var owl = $(".carrousel_fourth");
    owl.owlCarousel({
        loop:true,
	    margin:0,
	    nav:false,
	    items: 2,
	    autoplay:true,
	    autoplayTimeout: 5000
    });
});

function changeCarosuel(_id){;
	$('section.third .content .container .menu').removeClass('active');
	$('#productionsMenu_'+_id).addClass('active');			
	$('.carrousel_third').trigger('destroy.owl.carousel');

	$('.carrousel_third').css('display', 'none');
    $('#carrousel_third_'+_id).css('display', 'flex');

	var owl = $("#carrousel_third_"+_id);
    owl.owlCarousel({
        loop:true,
	    margin:0,
	    nav:false,
	    items: 4,
	    autoplay:true,
	    autoplayTimeout: 5000,
	    autoplayHoverPause: true
    });

     $("#third-owl-next").click(function(){
	  owl.trigger('next.owl');
	});
	$("#third-owl-prev").click(function(){
	  owl.trigger('prev.owl');
	});
}

$('.order_price').click(function(){
	$('#buyProductDescription').html('');
	$('#buyProductOldPrice').html('');

	var descriptionText = $(this).parent('div').parent('div').find('.name_text').html();
	var nameText = $(this).parent('div').parent('div').find('.title_name_text').html();
	var oldPrice = $(this).parent('div').parent('div').find('.old_price').html();
	var priceText = $(this).parent('div').parent('div').find('.price_text').html();
	var imageSrc = $(this).parent('div').parent('div').parent('div').find('.goods img').attr('src');
	
	$('#buyProductImg').attr('src', imageSrc);
	$('#buyProductName').html(nameText);
	$('#buyProductDescription').html(descriptionText);
	$('#buyProductOldPrice').html(oldPrice);
	$('#buyProductPrice').html(priceText);

	var destination = $('#selected_products').offset().top;
	jQuery("html:not(:animated),body:not(:animated)").animate({scrollTop: destination}, 800);
});

$('#productsBlock').mouseover(function(){
	$('.order_price').click(function(){
		$('#buyProductDescription').html('');
		$('#buyProductOldPrice').html('');

		var descriptionText = $(this).parent('div').parent('div').find('.name_text').html();
		var nameText = $(this).parent('div').parent('div').find('.title_name_text').html();
		var oldPrice = $(this).parent('div').parent('div').find('.old_price').html();
		var priceText = $(this).parent('div').parent('div').find('.price_text').html();
		var imageSrc = $(this).parent('div').parent('div').parent('div').find('.goods img').attr('src');
		
		$('#buyProductImg').attr('src', imageSrc);
		$('#buyProductName').html(nameText);
		$('#buyProductDescription').html(descriptionText);
		$('#buyProductOldPrice').html(oldPrice);
		$('#buyProductPrice').html(priceText);

		var destination = $('#selected_products').offset().top;
		jQuery("html:not(:animated),body:not(:animated)").animate({scrollTop: destination}, 800);
	});
});

$('#secondProductsBlock').mouseover(function(){
	$('.order_price').click(function(){
		$('#buyProductDescription').html('');
		$('#buyProductOldPrice').html('');

		var descriptionText = $(this).parent('div').parent('div').find('.name_text').html();
		var nameText = $(this).parent('div').parent('div').find('.title_name_text').html();
		var oldPrice = $(this).parent('div').parent('div').find('.old_price').html();
		var priceText = $(this).parent('div').parent('div').find('.price_text').html();
		var imageSrc = $(this).parent('div').parent('div').parent('div').find('.goods img').attr('src');
		
		$('#buyProductImg').attr('src', imageSrc);
		$('#buyProductName').html(nameText);
		$('#buyProductDescription').html(descriptionText);
		$('#buyProductOldPrice').html(oldPrice);
		$('#buyProductPrice').html(priceText);

		var destination = $('#selected_products').offset().top;
		jQuery("html:not(:animated),body:not(:animated)").animate({scrollTop: destination}, 800);
	});
});

function header_is(){
    if($('#is_select_block').css('display') == 'block') {
        $('#is_select_block').css('display', 'none');
        $('#is_select_hidden_block').css('display', 'none');
    }else{
        $('#is_select_block').css('display', 'block');
        $('#is_select_hidden_block').css('display', 'block');
    }
}

function select_is(_id){
	var selected = $('#is_select_'+_id).html();
	$('#is_select_0').html(selected);
	$('#buyFormColor').val(selected);

    $('#is_select_block').css('display', 'none');
    $('#is_select_hidden_block').css('display', 'none');
}

function select_isNap(_id){
	var selected = $('#is_select_blockNap_'+_id).html();
	$('#is_selectNap_0').html(selected);
	$('#buyFormNapovn').val(selected);

    $('#is_select_blockNap').css('display', 'none');
    $('#is_select_hidden_blockNap').css('display', 'none');
}

function header_isNap(){
    if($('#is_select_blockNap').css('display') == 'block') {
        $('#is_select_blockNap').css('display', 'none');
        $('#is_select_hidden_blockNap').css('display', 'none');
    }else{
        $('#is_select_blockNap').css('display', 'block');
        $('#is_select_hidden_blockNap').css('display', 'block');
    }
}

$('.model_balls').click(function(){
	var id = $(this).find('.number').html();
	select_is(id);
	$('#modalColors').modal('hide');
});

$('#orderCount').change(function(){
	var count = $('#orderCount').val(),
		price = $('#buyProductPrice span').html(),
		product = $('#buyProductName').html(), 
		color = $('#is_select_0').html(),
		napovn = $('#is_selectNap_0').html();

	summ = count * price;
	$('#orederSumm').html(summ);
	$('#buyFormProduct').val(product);
	$('#buyFormPrice').val(price);
	$('#buyFormNapovn').val(napovn);
	$('#buyFormColor').val(color);
});
$('#buyProductPrice').bind("DOMSubtreeModified", function(){
	var count = $('#orderCount').val(),
		price = $('#buyProductPrice span').html(),
		product = $('#buyProductName').html(), 
		color = $('#is_select_0').html(),
		napovn = $('#is_selectNap_0').html();

	summ = count * price;
	if(summ > 0){
		$('#orederSumm').html(summ);
		$('#buyFormProduct').val(product);
		$('#buyFormPrice').val(price);
		$('#buyFormNapovn').val(napovn);
		$('#buyFormColor').val(color);
	}
});

$(".btn.add").click(function(){
	var action = $(this).find("label").attr('class');
	window.location.href = '?r=admin/'+action+'&add';
});

$(".divTable .line").click(function(){
	var id = $(this).find(".cell.id span").html(),
		action = $(this).find(".cell.id span").attr('class');
	window.location.href = '?r=admin/'+action+'&edit&id='+id;
});

$('#alertBlockClose').click(function(){
	$('#alertBlock').fadeOut(700);
});

$(document).ready(function(){
  	var html = $('#alertBlockText').html(),
		_len = 0;
	_len = html.length;
	if(_len != 0){
		$('#alertBlock').fadeIn(700);
	    setTimeout(function(){
	        $('#alertBlock').fadeOut(700);
	    }, 7000);
	}
});

